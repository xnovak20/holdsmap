import type { Config } from 'tailwindcss';
import colors from 'tailwindcss/colors';
import { withUt } from 'uploadthing/tw';

export default withUt({
	content: [
		'./pages/**/*.{ts,tsx}',
		'./components/**/*.{ts,tsx}',
		'./app/**/*.{ts,tsx}',
		'./src/**/*.{ts,tsx,mdx}',
	],
	prefix: '',
	theme: {
		container: {
			center: true,
			padding: '2rem',
			screens: {
				'2xl': '1400px',
			},
		},
		extend: {
			colors: {
				primary: colors.blue['500'],
				'primary-hover': colors.blue['600'],

				secondary: colors.cyan['500'],
				'secondary-hover': colors.cyan['600'],

				destructive: colors.red['500'],
				'destructive-hover': colors.red['600'],

				warning: colors.yellow['500'],
				'warning-hover': colors.yellow['600'],

				success: colors.green['500'],
				'success-hover': colors.green['600'],
			},
			keyframes: {
				'accordion-down': {
					from: { height: '0' },
					to: { height: 'var(--radix-accordion-content-height)' },
				},
				'accordion-up': {
					from: { height: 'var(--radix-accordion-content-height)' },
					to: { height: '0' },
				},
				move: {
					to: {
						strokeDashoffset: '1000',
					},
				},
			},
			animation: {
				'accordion-down': 'accordion-down 0.2s ease-out',
				'accordion-up': 'accordion-up 0.2s ease-out',
			},
		},
	},
	plugins: [require('tailwindcss-animate')],
} satisfies Config);
