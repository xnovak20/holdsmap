import type { Config } from 'drizzle-kit';

export default {
	dialect: 'sqlite',
	schema: './src/db/schema',
	driver: 'turso',
	dbCredentials: {
		url: 'libsql://drizzle-demo24-cudnysanta.turso.io',
		// url: 'http://127.0.0.1:8080',
		authToken:
			'eyJhbGciOiJFZERTQSIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE3MTcxOTI5MjcsImlkIjoiMmY3ODdlYzctNzUyNy00N2UxLWIyYTUtMjAzMmQ5ZjA5OWFmIn0.r4-_SlkE9yHHpk2_jecijacTIMtIHmpUUVPMxp2I48DY-LLmpGzNPZiCB6c5eXL66qZbZhHee-xlkgASeFjNCA',
	},
	out: './migrations',
} satisfies Config;
