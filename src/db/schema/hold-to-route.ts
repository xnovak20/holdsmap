import { sqliteTable, text } from 'drizzle-orm/sqlite-core';
import { route } from './route';
import { hold, roleSchema } from './hold';

export const holdToRoute = sqliteTable('holdsToRoute', {
	holdId: text('holdId')
		.notNull()
		.references(() => hold.id),
	routeId: text('routeId')
		.notNull()
		.references(() => route.id),
	role: text('role', { enum: roleSchema.options }).notNull(),
});
