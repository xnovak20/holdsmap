import { integer, sqliteTable, text } from 'drizzle-orm/sqlite-core';
import { gym } from './gym';

export const wall = sqliteTable('wall', {
	id: text('id').primaryKey(),
	name: text('name').notNull(),
	picture: text('picture').notNull(),
	createdAt: integer('createdAt', { mode: 'timestamp_ms' }).notNull(),
	deletedAt: integer('deletedAt', { mode: 'timestamp_ms' }),
	gymId: text('gymId').references(() => gym.id),
});
