import { integer, sqliteTable, text } from 'drizzle-orm/sqlite-core';
import { relations, sql } from 'drizzle-orm';
import { user } from '@/db/schema/user';
import { gymToUserBookmark } from '@/db/schema/gym-to-user-bookmark';

export const gym = sqliteTable('gym', {
	id: text('id').primaryKey(),
	name: text('name').notNull(),
	private: integer('isPrivate', { mode: 'boolean' }).notNull(),
	location: text('location'),
	description: text('description'),
	news: text('news'),
	ownerId: text('ownerId'),
	createdAt: integer('createdAt', { mode: 'timestamp_ms' })
		.default(sql`CURRENT_TIMESTAMP`)
		.notNull(),
	deletedAt: integer('deletedAt', { mode: 'timestamp_ms' }),
});

export const gymRelations = relations(gym, ({ one, many }) => ({
	user: one(user, {
		fields: [gym.ownerId],
		references: [user.id],
	}),
	gymToUserBookmark: many(gymToUserBookmark),
}));
