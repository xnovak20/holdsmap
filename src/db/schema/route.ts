import { integer, real, sqliteTable, text } from 'drizzle-orm/sqlite-core';
import { wall } from './wall';
import { user } from './user';

export const route = sqliteTable('route', {
	id: text('id').primaryKey(),
	name: text('name').notNull(),
	description: text('description'),
	grade: real('grade'),
	createdAt: integer('createdAt', { mode: 'timestamp_ms' }).notNull(),
	deletedAt: integer('deletedAt', { mode: 'timestamp_ms' }),
	wallId: text('wallId')
		.notNull()
		.references(() => wall.id),
	userId: text('userId')
		.notNull()
		.references(() => user.id),
});
