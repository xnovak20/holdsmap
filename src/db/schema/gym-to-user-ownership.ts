import { sqliteTable, text } from 'drizzle-orm/sqlite-core';
import { user } from '@/db/schema/user';
import { gym } from '@/db/schema/gym';

export const gymToUserOwnership = sqliteTable('gymToUserOwnership', {
	gymId: text('gymId')
		.notNull()
		.references(() => gym.id),
	userId: text('userId')
		.notNull()
		.references(() => user.id),
});
