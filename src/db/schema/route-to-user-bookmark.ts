import { sqliteTable, text } from 'drizzle-orm/sqlite-core';
import { user } from '@/db/schema/user';
import { route } from '@/db/schema/route';

export const routeToUserBookmark = sqliteTable('routesToUserBookmark', {
	routeId: text('routeId')
		.notNull()
		.references(() => route.id),
	userId: text('userId')
		.notNull()
		.references(() => user.id),
});
