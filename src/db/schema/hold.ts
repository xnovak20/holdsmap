import { integer, sqliteTable, text } from 'drizzle-orm/sqlite-core';
import { wall } from './wall';
import { z } from 'zod';

export const roleSchema = z.enum(['hold', 'foothold', 'start', 'top']);
export const typeSchema = z.enum(['hold', 'volume']);

export const hold = sqliteTable('hold', {
	id: text('id').primaryKey(),
	type: text('type', { enum: typeSchema.options }).notNull(),
	createdAt: integer('createdAt', { mode: 'timestamp_ms' }).notNull(),
	deletedAt: integer('deletedAt', { mode: 'timestamp_ms' }),
	wallId: text('wallId')
		.notNull()
		.references(() => wall.id),
});
