import { text, sqliteTable, real, integer } from 'drizzle-orm/sqlite-core';
import { user } from './user';
import { route } from './route';

export const ascent = sqliteTable('ascent', {
	id: text('id').primaryKey(),
	attempts: integer('attempts').notNull(),
	gradeOpinion: real('gradeOpinion'),
	stars: integer('stars'),
	sentAt: integer('sentAt', { mode: 'timestamp_ms' }),
	userId: text('userId').references(() => user.id),
	routeId: text('routeId').references(() => route.id),
});
