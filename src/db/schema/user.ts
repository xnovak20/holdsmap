import { integer, sqliteTable, text } from 'drizzle-orm/sqlite-core';
import { InferSelectModel, relations } from 'drizzle-orm';
import { gym } from '@/db/schema/gym';
import { gymToUserBookmark } from '@/db/schema/gym-to-user-bookmark';

export type User = InferSelectModel<typeof user>;

export const user = sqliteTable('user', {
	id: text('id').notNull().primaryKey(),
	name: text('name').notNull(),
	surname: text('surname').notNull(),
	email: text('email').notNull(),
	password: text('password').notNull(),
	createdAt: integer('createdAt', { mode: 'timestamp_ms' }).notNull(),
	deletedAt: integer('deletedAt', { mode: 'timestamp_ms' }),
});

export const userRelations = relations(user, ({ many }) => ({
	gyms: many(gym),
	gymToUserBookmark: many(gymToUserBookmark),
}));
