import { real, sqliteTable, text } from 'drizzle-orm/sqlite-core';
import { hold } from './hold';

export const point = sqliteTable('point', {
	id: text('id').primaryKey(),
	x: real('x').notNull(),
	y: real('y').notNull(),
	holdId: text('holdId')
		.notNull()
		.references(() => hold.id),
});
