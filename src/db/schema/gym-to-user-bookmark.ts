import { sqliteTable, text } from 'drizzle-orm/sqlite-core';
import { user } from '@/db/schema/user';
import { gym } from '@/db/schema/gym';
import { relations } from 'drizzle-orm';

export const gymToUserBookmark = sqliteTable('gymsToUserBookmark', {
	gymId: text('gymId')
		.notNull()
		.references(() => gym.id),
	userId: text('userId')
		.notNull()
		.references(() => user.id),
});

export const gymToUserBookmarkRelations = relations(
	gymToUserBookmark,
	({ one }) => ({
		user: one(user, {
			fields: [gymToUserBookmark.userId],
			references: [user.id],
		}),
		gym: one(gym, {
			fields: [gymToUserBookmark.gymId],
			references: [gym.id],
		}),
	})
);
