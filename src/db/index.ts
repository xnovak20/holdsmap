import { drizzle } from 'drizzle-orm/libsql';
import { createClient } from '@libsql/client';
import { wall } from './schema/wall';
import { user, userRelations } from './schema/user';
import { ascent } from './schema/ascent';
import { hold } from './schema/hold';
import { route } from './schema/route';
import { gym, gymRelations } from './schema/gym';
import {
	gymToUserBookmark,
	gymToUserBookmarkRelations,
} from './schema/gym-to-user-bookmark';
import { gymToUserOwnership } from './schema/gym-to-user-ownership';
import { routeToUserBookmark } from './schema/route-to-user-bookmark';
import { point } from './schema/point';

const client = createClient({
	url: 'libsql://drizzle-demo24-cudnysanta.turso.io',
	authToken:
		'eyJhbGciOiJFZERTQSIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE3MTcxOTI5MjcsImlkIjoiMmY3ODdlYzctNzUyNy00N2UxLWIyYTUtMjAzMmQ5ZjA5OWFmIn0.r4-_SlkE9yHHpk2_jecijacTIMtIHmpUUVPMxp2I48DY-LLmpGzNPZiCB6c5eXL66qZbZhHee-xlkgASeFjNCA',
});

export const db = drizzle(client, {
	schema: {
		ascent,
		route,
		user,
		gym,
		wall,
		hold,
		point,
		gymToUserBookmark,
		gymToUserOwnership,
		routeToUserBookmark,

		// Relations
		userRelations,
		gymRelations,
		gymToUserBookmarkRelations,
	},
});
