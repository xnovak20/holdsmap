import { faker } from '@faker-js/faker';
import { user } from '@/db/schema/user';
import { Factory } from '@/db/factories/factory';

export const userFactory: Factory<typeof user> = () => {
	const firstName = faker.person.firstName();
	const lastName = faker.person.lastName();

	return {
		id: faker.string.uuid(),
		email: faker.internet.email({ firstName, lastName }),
		name: firstName,
		surname: lastName,
		password: faker.internet.password(),
		createdAt: new Date(),
		deletedAt: null,
	};
};

export default userFactory;
