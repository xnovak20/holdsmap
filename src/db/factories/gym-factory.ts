import { faker, fakerCS_CZ } from '@faker-js/faker';
import { gym } from '@/db/schema/gym';
import { Factory } from '@/db/factories/factory';

const gymFactory: Factory<typeof gym> = () => ({
	id: faker.string.uuid(),
	name: fakerCS_CZ.company.name(),
	description: faker.lorem.paragraph(),
	location: fakerCS_CZ.location.city(),
	private: faker.datatype.boolean(),
	news: faker.lorem.paragraph(),
	createdAt: new Date(),
	deletedAt: null,
});

export default gymFactory;
