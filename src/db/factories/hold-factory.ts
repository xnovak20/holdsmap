import { Factory } from '@/db/factories/factory';
import { hold, roleSchema, typeSchema } from '@/db/schema/hold';
import { faker } from '@faker-js/faker';

const holdFactory: Factory<typeof hold> = () => {
	return {
		id: faker.string.uuid(),
		role: faker.helpers.arrayElement(roleSchema.options),
		type: faker.helpers.arrayElement(typeSchema.options),
		createdAt: new Date(),
	};
};

export default holdFactory;
