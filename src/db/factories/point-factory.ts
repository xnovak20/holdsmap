import { faker } from '@faker-js/faker';
import { Factory } from '@/db/factories/factory';
import { point } from '@/db/schema/point';

export const pointFactory: Factory<typeof point> = () => ({
	id: faker.string.uuid(),
	x: faker.number.int({ min: 0, max: 500 }),
	y: faker.number.int({ min: 0, max: 500 }),
});

export default pointFactory;
