import { faker } from '@faker-js/faker';
import { Factory } from '@/db/factories/factory';
import { wall } from '@/db/schema/wall';

const wallFactory: Factory<typeof wall> = () => ({
	id: faker.string.uuid(),
	name: faker.word.words({ count: 1 }),
	picture: faker.image.url(),
	createdAt: new Date(),
	deletedAt: null,
});

export default wallFactory;
