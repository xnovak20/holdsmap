import { Table } from 'drizzle-orm';
import { db } from '@/db';

export type Factory<TTable extends Table> = () => Partial<
	TTable['$inferSelect']
>;

export type FactoryData<TTable extends Table> = TTable['$inferSelect'];

export const factory = async <TTable extends Table>(
	table: TTable,
	factory: Factory<TTable>,
	{
		count = 1,
		attributes = undefined,
		insert = true,
	}: {
		count?: number;
		attributes?: Partial<FactoryData<TTable>>;
		insert?: boolean;
	} = {}
) => {
	const data: FactoryData<TTable>[] = [];

	if (count < 1) {
		throw new Error('Count cannot be less than 1.');
	}

	for (let i = 0; i < count ?? 1; i++) {
		const object = factory();

		if (attributes) {
			Object.keys(attributes).map((k) => {
				const attribute = attributes[k];

				if (attribute !== undefined && attribute !== null) {
					object[k as keyof FactoryData<TTable>] = attribute;
				}
			});
		}

		data.push(object);
	}

	const flatted = data.flat(0);

	if (insert) {
		await db.insert(table).values(flatted);
	}

	return flatted;
};
