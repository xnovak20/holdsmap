import { FactoryData } from '@/db/factories/factory';
import { Table } from 'drizzle-orm';
import { db } from '@/db';
import { user } from '@/db/schema/user';
import { point } from '@/db/schema/point';
import { hold } from '@/db/schema/hold';
import { wall } from '@/db/schema/wall';
import { gym } from '@/db/schema/gym';
import userSeeder from '@/db/seeders/user-seeder';
import gymSeeder from '@/db/seeders/gym-seeder';
import wallSeeder from '@/db/seeders/wall-seeder';
import holdSeeder from '@/db/seeders/hold-seeder';
import pointSeeder from '@/db/seeders/point-seeder';

export type Seeder<TTable extends Table> = (
	...args: any[]
) => Promise<FactoryData<TTable>[] | void>;

const seed = async () => {
	console.info('Deleting all tables within a seed.');

	await db.delete(user);
	await db.delete(point);
	await db.delete(hold);
	await db.delete(wall);
	await db.delete(gym);

	console.info('Seeding tables.');

	try {
		console.info('Seeding users');
		const users = await userSeeder();
		console.info('Seeding gyms');
		const gyms = await gymSeeder(users);
		console.info('Seeding gyms');
		const walls = await wallSeeder(gyms);
		console.info('Seeding holds');
		const holds = await holdSeeder(walls);
		console.info('Seeding points');
		await pointSeeder(holds);
	} catch (e) {
		console.error('Seeding failed:');
		console.error(e);
	}
};

seed()
	.then(() => console.info('Seeding done.'))
	.catch((error) => {
		console.error('Seed failed:');
		console.error(error);
	});
