import { factory, FactoryData } from '@/db/factories/factory';
import { gym } from '@/db/schema/gym';
import gymFactory from '@/db/factories/gym-factory';
import { Seeder } from '@/db/seeders/seeder';
import { user } from '@/db/schema/user';
import { faker } from '@faker-js/faker';

const gymSeeder: Seeder<typeof gym> = async (
	users: FactoryData<typeof user>[]
) =>
	await factory(gym, gymFactory, {
		count: 10,
		attributes: {
			ownerId: faker.helpers.arrayElement(users.map((user) => user.id)),
		},
	});

export default gymSeeder;
