import { factory, FactoryData } from '@/db/factories/factory';
import { gym } from '@/db/schema/gym';
import { wall } from '@/db/schema/wall';
import wallFactory from '@/db/factories/wall-factory';
import { faker } from '@faker-js/faker';
import { Seeder } from '@/db/seeders/seeder';

const wallSeeder: Seeder<typeof wall> = async (
	gyms: FactoryData<typeof gym>[]
) => {
	const data: FactoryData<typeof wall>[] = [];

	await Promise.all(
		gyms.map(async (gym) => {
			data.push(
				...(await factory(wall, wallFactory, {
					count: faker.number.int({ min: 1, max: 4 }),
					attributes: {
						gymId: gym.id,
					},
				}))
			);
		})
	);

	return data;
};

export default wallSeeder;
