import { factory, FactoryData } from '@/db/factories/factory';
import { wall } from '@/db/schema/wall';
import { faker } from '@faker-js/faker';
import { hold } from '@/db/schema/hold';
import holdFactory from '@/db/factories/hold-factory';
import { Seeder } from '@/db/seeders/seeder';

const holdSeeder: Seeder<typeof hold> = async (
	walls: FactoryData<typeof wall>[]
) =>
	await factory(hold, holdFactory, {
		count: faker.number.int({ min: 5, max: 15 }),
		attributes: {
			wallId: faker.helpers.arrayElement(walls.map((wall) => wall.id)),
		},
	});

export default holdSeeder;
