import { factory } from '@/db/factories/factory';
import { user } from '@/db/schema/user';
import userFactory from '@/db/factories/user-factory';
import { Seeder } from '@/db/seeders/seeder';

const userSeeder: Seeder<typeof user> = async () => [
	...(await factory(user, userFactory, {
		attributes: {
			email: 'test@test.cz',
			name: 'Test',
			surname: 'Test',
			password: 'test',
		},
	})),
	...(await factory(user, userFactory, {
		count: 2,
		attributes: {
			password: 'password',
		},
	})),
];

export default userSeeder;
