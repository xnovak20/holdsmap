import { factory, FactoryData } from '@/db/factories/factory';
import { faker } from '@faker-js/faker';
import { hold } from '@/db/schema/hold';
import { point } from '@/db/schema/point';
import pointFactory from '@/db/factories/point-factory';
import { Seeder } from '@/db/seeders/seeder';

const pointSize = 4;

const pointSeeder: Seeder<typeof point> = async (
	holds: FactoryData<typeof hold>[]
) => {
	const points: FactoryData<typeof point>[] = [];

	holds.map(async (hold) => {
		let coordinates: { x: number; y: number } = {
			x: faker.number.int({
				min: pointSize / 2,
				max: 500 - pointSize / 2,
			}),
			y: faker.number.int({
				min: pointSize / 2,
				max: 500 - pointSize / 2,
			}),
		};
		let direction: 'right' | 'down' | 'left' | 'up' = 'right';

		for (let i = 0; i < 4; i++) {
			if (direction === 'right') {
				coordinates.x += pointSize / 2;
				direction = 'down';
			} else if (direction === 'down') {
				coordinates.y -= pointSize / 2;
				direction = 'left';
			} else if (direction === 'left') {
				coordinates.x -= pointSize / 2;
			} else if (direction === 'up') {
				coordinates.y += pointSize / 2;
			}

			const createdPoint = (
				await factory(point, pointFactory, {
					attributes: {
						...coordinates,
						holdId: hold.id,
					},
				})
			)[0];

			points.push(createdPoint);
		}
	});

	return points;
};

export default pointSeeder;
