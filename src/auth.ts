import NextAuth from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';
import Google from 'next-auth/providers/google';
import { redirect } from 'next/navigation';
import { revalidatePath } from 'next/cache';
import _config from '@/config/app';
import userRepository from '@/repository/user-repository';

type CredentialsProps = {
	email: string;
	password: string;
};

export const { handlers, signIn, signOut, auth } = NextAuth({
	providers: [
		Google,
		CredentialsProvider({
			credentials: {
				email: {},
				password: {},
			},
			authorize: async (credentials) =>
				await authenticate(credentials as CredentialsProps),
		}),
	],
	callbacks: {
		async signIn({ account, profile }) {
			// The provider won't save the user into the database unless the adapter
			// is used, but for the simplicity we created signIn function, which will
			// store the non-existent user to the database.
			if (
				account?.provider === 'google' &&
				profile &&
				isCreatableProfile(profile.email)
			) {
				// Create user if not found in database
				if (!(await userRepository.getByEmail(profile.email))) {
					await userRepository.create({
						email: profile.email,
						name: profile.given_name ?? '',
						surname: profile.family_name ?? '',
						password: '',
					});
				}
			}

			return true;
		},
		async redirect({ url, baseUrl }: { url: string; baseUrl: string }) {
			return '/gyms';
		},
	},
	pages: {
		signIn: _config.notAuthenticatedRedirectPath,
	},
});

/**
 * Get the authenticated user, or redirect to login page if not authenticated
 */
export const getAuthenticatedUser = async () => {
	const session = await auth();

	// Either the session or email is not set
	if (!session || !session?.user?.email) {
		return redirect(_config.notAuthenticatedRedirectPath);
	}

	// Get the user from the storage
	const user = await userRepository.getByEmail(session.user.email);

	// If the user not found signOut the user (to destroy current session),
	// revalidate data and redirect back to the login page
	if (!user) {
		await signOut();

		revalidatePath('/', 'layout');

		return redirect(_config.notAuthenticatedRedirectPath);
	}

	// Return user from the storage
	return user;
};

/**
 * Authenticate the user via the credentials.
 */
const authenticate = async (credentials: CredentialsProps) => {
	const user = await userRepository.matchCredentials(credentials);

	if (!user) {
		throw new Error('Incorrect credentials.');
	}

	return user;
};

/**
 * Check if the profile has defined email.
 */
const isCreatableProfile = (email: unknown): email is string => {
	return typeof email === 'string' && email.length > 0;
};
