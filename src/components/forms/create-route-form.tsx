'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import { FormProvider, useForm } from 'react-hook-form';
import { z } from 'zod';

import { Button } from '@/components/ui/button';
import { addRouteAction } from '@/actions/add-route-action';
import React from 'react';
import { useParams, useRouter } from 'next/navigation';
import { FormInput } from '@/components/ui/form-controls/form-input';
import { FormTextarea } from '@/components/ui/form-controls/form-textarea';
import { RouteHold } from '@/types/hold-type';

const formSchema = z.object({
	name: z.string().min(3),
	description: z.string().optional(),
	grade: z.coerce.number().nonnegative(),
});

type FormSchema = z.infer<typeof formSchema>;

type CreateRouteFormProps = {
	wallId: string;
	holds: RouteHold[];
};

export const AddRouteForm = (props: CreateRouteFormProps) => {
	const router = useRouter();
	const params = useParams<{ gymId: string }>();
	const form = useForm<FormSchema>({
		resolver: zodResolver(formSchema),
	});

	const handleAddRoute = async (values: FormSchema) => {
		//const user = await getAuthenticatedUser()
		const newRouteId = await addRouteAction(
			values.name,
			values.description,
			values.grade,
			props.wallId,
			/*user.id*/ '35bea7c5-3267-4fd3-aaf0-1332c3c9a633', //todo remove hardcoded
			props.holds
		);
		form.reset();
		router.push(`/gyms/${params.gymId}/climbs/${newRouteId}`);
	};

	return (
		<FormProvider {...form}>
			<form
				onSubmit={form.handleSubmit(handleAddRoute)}
				className="space-y-4"
			>
				<FormInput type={'text'} label={'Name'} name={'name'} />
				<FormTextarea label="Desription" name="description" />
				<FormInput type={'number'} label={'Grade'} name={'grade'} />
				<Button type="submit">Add route</Button>
			</form>
		</FormProvider>
	);
};
