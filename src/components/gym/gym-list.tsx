import { FC } from 'react';
import { User } from '@/db/schema/user';
import gymRepository from '@/repository/gym-repository';
import { GymItem } from '@/components/gym/gym-item';
import { Separator } from '@/components/ui/separator';

type Props = {
	query?: string;
	user: User;
};

export const GymList: FC<Props> = async ({ query, user }) => {
	const gyms = await gymRepository.all({ q: query });

	const bookmarked = gyms.filter((gym) =>
		gym.gymToUserBookmark.find(
			(gu) => gu.userId === user.id && gu.gymId === gym.id
		)
	);

	const owned = gyms.filter(
		(gym) => gym.ownerId === user.id && !bookmarked.includes(gym)
	);

	const rest = gyms.filter(
		(gym) => !bookmarked.includes(gym) && !owned.includes(gym)
	);

	return (
		<div className="space-y-10 lg:space-y-12">
			{bookmarked.length > 0 && (
				<>
					<div>
						<h1 className="mb-2 text-xl font-semibold">
							Favorites
						</h1>
						<div className="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-3">
							{bookmarked.map((gym) => (
								<GymItem
									key={gym.id}
									gym={gym}
									user={user}
									bookmarked
									owned={user.id === gym.ownerId}
								/>
							))}
						</div>
					</div>
					<Separator />
				</>
			)}

			{owned.length > 0 && (
				<>
					<div>
						<h1 className="mb-2 text-xl font-semibold">
							Your gyms
						</h1>
						<div className="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-3">
							{owned.map((gym) => (
								<GymItem
									key={gym.id}
									gym={gym}
									user={user}
									owned
								/>
							))}
						</div>
					</div>
					<Separator />
				</>
			)}

			<div className="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-3">
				{rest.map((gym) => (
					<GymItem key={gym.id} gym={gym} user={user} />
				))}
			</div>
		</div>
	);
};
