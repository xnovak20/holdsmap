'use client';

import { Gym } from '@/types/gym';

const GymDashboard = ({ gym, owner }: { gym: Gym; owner: any }) => {
	return (
		<div className="container mx-auto py-8">
			<h1 className="mb-4 text-3xl font-bold">{gym.name}</h1>
			<p className="mb-4 text-lg">{gym.description}</p>
			<p className="mb-4 text-lg">{gym.location}</p>
			{gym.news && (
				<div className="mb-8 rounded-lg bg-white p-6 shadow-md">
					<h2 className="mb-4 text-2xl font-semibold">Latest News</h2>
					<p className="text-lg">{gym.news}</p>
				</div>
			)}
			{owner ? (
				<a href={`/gyms/${gym.id}/edit`} className="button">
					Edit
				</a>
			) : (
				<p>Edit nepovolen</p>
			)}

			<div className="grid grid-cols-1 gap-8 md:grid-cols-2">
				<a
					href={`/gyms/${gym.id}/walls`}
					className="flex items-center justify-center rounded-xl bg-blue-500 px-10 py-8 text-white hover:bg-blue-600"
				>
					<span className="text-2xl font-semibold">Gym Walls</span>
				</a>

				<a
					href={`/gyms/${gym.id}/routes`}
					className="flex items-center justify-center rounded-xl bg-green-500 px-10 py-8 text-white hover:bg-green-600"
				>
					<span className="text-2xl font-semibold">Gym Routes</span>
				</a>
			</div>
		</div>
	);
};
export default GymDashboard;
