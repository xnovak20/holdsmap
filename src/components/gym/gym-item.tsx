'use client';

import { BookmarkCheck, BookmarkPlus } from 'lucide-react';
import { FetchedGym } from '@/types/gym-type';
import { updateGymBookmarkAction } from '@/actions/update-gym-bookmark-action';
import { MouseEventHandler } from 'react';
import { User } from '@/db/schema/user';
import Link from 'next/link';
import { cn } from '@/lib/utils';

type GymItemProps = {
	gym: FetchedGym;
	user: User;
	bookmarked?: boolean;
	owned?: boolean;
};

export const GymItem = ({
	gym: { id, location, name, private: isPrivate },
	user,
	bookmarked = false,
	owned = false,
}: GymItemProps) => {
	const handleChange: MouseEventHandler<HTMLButtonElement> = async (
		event
	) => {
		event.stopPropagation();

		await updateGymBookmarkAction(user.id, id);
	};

	const BookmarkComponent = bookmarked ? BookmarkCheck : BookmarkPlus;

	return (
		<div className="relative flex gap-2 rounded-xl border border-gray-100 px-4 py-5 shadow transition-all hover:scale-105 hover:shadow-lg">
			<div className="relative z-10 flex flex-col justify-center">
				<button onClick={handleChange}>
					<BookmarkComponent
						className={cn(
							'size-8 text-success transition-all hover:scale-105 lg:size-12',
							bookmarked
								? 'text-warning hover:text-destructive'
								: 'text-gray-200 hover:text-warning'
						)}
					/>
				</button>
			</div>
			<div className="flex flex-col items-start justify-center">
				<h1 className="text-base font-semibold lg:text-lg">{name}</h1>
				{!isPrivate && (
					<div className="text-xs md:text-sm">{location}</div>
				)}
			</div>
			{owned && (
				<div className="absolute right-2 top-2 rounded-full bg-primary/10 px-2 py-0.5 text-xs text-primary">
					Yours
				</div>
			)}
			<Link href={`/gyms/${id}`} className="absolute inset-0" />
		</div>
	);
};
