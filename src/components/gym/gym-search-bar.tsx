'use client';

import { Command, CommandInput, CommandList } from '@/components/ui/command';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import { useDebounceCallback } from 'usehooks-ts';

export const GymSearchBar = () => {
	const searchParams = useSearchParams();
	const pathname = usePathname();
	const { replace } = useRouter();

	const handleSearch = useDebounceCallback((q: string) => {
		const params = new URLSearchParams(searchParams);

		if (q.length > 0) {
			params.set('q', q);
		} else {
			params.delete('q');
		}

		replace(`${pathname}?${params.toString()}`);
	}, 500);

	return (
		<Command className="w-full rounded-lg border border-solid border-black">
			<CommandInput
				onValueChange={handleSearch}
				placeholder="Search..."
			/>
			<CommandList />
		</Command>
	);
};
