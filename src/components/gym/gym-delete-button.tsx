'use client';
import DeleteGymAction from '@/actions/gym/delete-gym-action';
import { Button } from '@/components/ui/button'; // Adjust this import as needed
import { useRouter } from 'next/navigation';
import { toast } from 'sonner';

const GymDeleteButton = ({ gymId }: { gymId: string }) => {
	const router = useRouter();
	const handleDelete = async () => {
		const response = await DeleteGymAction(gymId);

		if (response) {
			toast.success('Gym deleted successfully.');
			router.push('/gyms');
		} else {
			toast.error(`Failed to delete the gym: ${gymId}`);
		}
	};

	return (
		<Button
			onClick={handleDelete}
			variant="secondary"
			className={'w-full bg-red-500 hover:bg-red-600'}
		>
			Delete Gym
		</Button>
	);
};

export default GymDeleteButton;
