'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import { FormProvider, useForm } from 'react-hook-form';
import { z } from 'zod';

import { createGymAction } from '@/actions/create-gym-action';
import { FormInput } from '@/components/ui/form-controls/form-input';
import { FormTextarea } from '@/components/ui/form-controls/form-textarea';
import { FormCheckbox } from '@/components/ui/form-controls/form-checkbox';
import { Button } from '@/components/ui/button';

const formSchema = z.object({
	name: z.string().min(1),
	description: z.string().optional(),
	news: z.string().optional(),
	location: z.string().optional(),
	private: z.boolean().default(false),
});

type FormSchema = z.infer<typeof formSchema>;

export const GymCreateForm = () => {
	const form = useForm<FormSchema>({
		resolver: zodResolver(formSchema),
	});

	const handleCreateGym = async (values: FormSchema) => {
		await createGymAction(values);
	};

	return (
		<FormProvider {...form}>
			<form
				onSubmit={form.handleSubmit(handleCreateGym)}
				className="space-y-4"
			>
				<FormInput type="text" label="Name" name="name" />
				<FormTextarea label="Description" name="description" />
				<FormTextarea label="News" name="news" />
				<FormInput type="text" label="Location" name="location" />
				<FormCheckbox
					label={{
						on: (
							<>
								Gym is marked as <strong>private</strong>
							</>
						),
						off: (
							<>
								Gym is marked as <strong>public</strong>
							</>
						),
					}}
					name="private"
				/>
				<Button type="submit" variant="primary" className="w-full">
					Create
				</Button>
			</form>
		</FormProvider>
	);
};
