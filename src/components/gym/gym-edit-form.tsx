'use client';

import updateGymAction from '@/actions/gym/update-gym-action';
import { Gym } from '@/types/gym';
import { zodResolver } from '@hookform/resolvers/zod';
import { FC } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { toast } from 'sonner';
import { z } from 'zod';
import { Button } from '../ui/button';
import { FormCheckbox } from '../ui/form-controls/form-checkbox';
import { FormTextarea } from '../ui/form-controls/form-textarea';
import { FormInput } from '@/components/ui/form-controls/form-input';
import DeleteGymAction from '@/actions/gym/delete-gym-action';
import { useRouter } from 'next/navigation';

const formSchema = z.object({
	name: z.string().min(1),
	location: z.string().optional(),
	description: z.string().optional(),
	news: z.string().optional(),
	private: z.boolean().default(false),
});

export type GymEditFormSchema = z.infer<typeof formSchema>;

type Props = {
	gym: Gym;
};

export const GymEditForm: FC<Props> = ({ gym }) => {
	const router = useRouter();
	const form = useForm<GymEditFormSchema>({
		resolver: zodResolver(formSchema),
		defaultValues: {
			name: gym.name,
			location: gym.location ?? undefined,
			description: gym.description ?? undefined,
			news: gym.news ?? undefined,
			private: gym.private,
		},
	});

	const handleUpdateGym = async (values: GymEditFormSchema) => {
		await updateGymAction(gym.id, values)
			.catch((error: Error) => toast.error(error.message))
			.then(() => toast.success('Gym updated successfully.'));
	};

	const handleDeleteGym = async () => {
		if (await DeleteGymAction(gym.id)) {
			toast.success('Gym deleted successfully.');
			router.push('/gyms');
		} else {
			toast.error('Failed to delete gym.');
		}
	};

	return (
		<FormProvider {...form}>
			<form
				onSubmit={form.handleSubmit(handleUpdateGym)}
				className="flex flex-col gap-4 p-4"
			>
				<FormInput type="text" label="Name" name="name" />
				<FormInput type="text" label="Location" name="location" />
				<FormTextarea label="Description" name="description" />
				<FormTextarea label="News" name="news" />
				<FormCheckbox
					label={{
						on: (
							<>
								Gym is marked as <strong>private</strong>
							</>
						),
						off: (
							<>
								Gym is marked as <strong>public</strong>
							</>
						),
					}}
					name="private"
				/>

				<div className="mt-4 space-y-4">
					<Button type="submit" variant="primary" className="w-full">
						Update
					</Button>
					{/*todo modal na potvrzeni, zda chce uzivatel opravdu smazat, ze je to nevratne*/}
					<Button
						type="button"
						onClick={handleDeleteGym}
						variant="destructive"
						className="w-full"
					>
						Delete
					</Button>
				</div>
			</form>
		</FormProvider>
	);
};
