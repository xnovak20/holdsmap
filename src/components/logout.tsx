'use client';

import { Button } from '@/components/ui/button';
import { logoutAction } from '@/actions/auth/logout-action';
import { LogOutIcon } from 'lucide-react';
import { useRouter } from 'next/navigation';

export function Logout() {
	const router = useRouter();
	const handleLogout = async () => {
		await logoutAction();
		router.push('/auth/login');
	};

	return (
		<form action={handleLogout}>
			<Button type="submit" className="text-white">
				<LogOutIcon className="size-10" />
			</Button>
		</form>
	);
}
