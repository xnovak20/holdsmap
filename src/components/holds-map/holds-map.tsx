'use client';

import React, { useEffect, useRef, useState } from 'react';
import { Hold, RouteHold } from '@/types/hold-type';
import { EditorMode } from '@/types/editor-mode-type';
import WallImage from '@/components/holds-map/wall-image';
import WallSvg from '@/components/holds-map/wall-svg';

type HoldsMapProps = {
	key?: number;
	image: string;
	wallId: string;
	mode: EditorMode;
	holds: Hold[];
	routeHolds: RouteHold[];
	onChangeHolds?: (holds: Hold[]) => void;
	onChangeRoute?: (updatedRouteHolds: RouteHold[]) => void;
};

export default function HoldsMap(props: HoldsMapProps) {
	const [dimensions, setDimensions] = useState({ width: 0, height: 0 });
	const parentRef = useRef<HTMLDivElement>(null);

	const handleResize = () => {
		if (parentRef.current) {
			const { clientWidth, clientHeight } = parentRef.current;
			setDimensions({ width: clientWidth, height: clientHeight });
		}
	};

	useEffect(() => {
		handleResize();
		setTimeout(function () {
			//todo fix this ugly workaround. The problem is that
			handleResize();
		}, 3000);
		window.addEventListener('resize', handleResize);
		return () => {
			window.removeEventListener('resize', handleResize);
		};
	}, []);

	return (
		<div className="relative w-full" ref={parentRef}>
			<WallImage image={props.image} />
			<div className="absolute left-0 top-0 flex h-full w-full items-center justify-center">
				<WallSvg
					mode={props.mode}
					parentHeight={dimensions.height}
					parentWidth={dimensions.width}
					onChangeHolds={props.onChangeHolds ?? (() => {})}
					onChangeRoute={props.onChangeRoute ?? (() => {})}
					holds={props.holds}
					routeHolds={props.routeHolds}
					wallId={props.wallId}
				/>
			</div>
		</div>
	);
}
