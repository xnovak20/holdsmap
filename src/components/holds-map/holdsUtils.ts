import { Point } from '@/types/point-type';
import { Hold, RouteHold } from '@/types/hold-type';
import { EditorMode } from '@/types/editor-mode-type';
import { HoldType } from '@/types/hold-type-type';

export type HoldToDraw = {
	id: string;
	polygon: Point[];
	color: string;
};

export const prepareToDraw = (
	wall: Hold[],
	route: RouteHold[],
	mode: EditorMode,
	type: HoldType,
	scaleWidth: number,
	scaleHeight: number
) => {
	const set: HoldToDraw[] = [];
	if (mode == 'readonly') {
		set.push(
			...route
				.filter((hold) => hold.type === type)
				.map((hold): HoldToDraw => {
					return {
						id: hold.id,
						polygon: hold.polygon,
						color: getHoldColor(hold),
					};
				})
		);
	} else if (mode == 'createVolumes' || mode == 'createHolds') {
		set.push(
			...wall
				.filter((hold) => hold.type === type)
				.map((hold): HoldToDraw => {
					return {
						id: hold.id,
						polygon: hold.polygon,
						color: getHoldColor(hold),
					};
				})
		);
	} else {
		set.push(
			...route
				.filter((hold) => hold.type === type)
				.map((hold): HoldToDraw => {
					return {
						id: hold.id,
						polygon: hold.polygon,
						color: getHoldColor(hold),
					};
				})
		);
		set.push(
			...wall
				.filter((hold) => hold.type === type)
				.map((hold): HoldToDraw => {
					return {
						id: hold.id,
						polygon: hold.polygon,
						color: 'rgba(0,0,0,0.01)',
					};
				})
		);
	}
	return set.map((hold) => {
		return {
			...hold,
			polygon: scaleHold(hold.polygon, scaleWidth, scaleHeight),
		};
	});
};

const getHoldColor = (hold: Hold | RouteHold): string => {
	if ('role' in hold) {
		switch (hold.role) {
			case 'hold':
				return 'yellow';
			case 'foothold':
				return 'blue';
			case 'start':
				return 'green';
			case 'top':
				return 'orange';
			default:
				return 'white';
		}
	} else {
		return 'white';
	}
};

export const scaleHold = (
	polygon: Point[],
	parentWidth: number,
	parentHeight: number
): Point[] => {
	return polygon.map((point) => ({
		x: point.x * parentWidth,
		y: point.y * parentHeight,
	}));
};
