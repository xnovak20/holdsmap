'use client';

import React, { MouseEventHandler } from 'react';
import { Coordinates } from '@/types/coordinates-type';

type StartCircleProps = {
	position: Coordinates;
	onClick: MouseEventHandler<SVGCircleElement>;
};

export default function StartCircle(props: StartCircleProps) {
	return (
		<circle
			cx={props.position.x}
			cy={props.position.y}
			r={5}
			fill="lime"
			fillOpacity={0.2}
			stroke="lime"
			strokeWidth={1.2}
			onClick={props.onClick}
		/>
	);
}
