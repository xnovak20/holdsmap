'use client';

import { RouteHold } from '@/types/hold-type';
import SvgPolygon from '@/components/holds-map/svg-volume';
import SvgBezierPath from '@/components/holds-map/svg-hold';

type SvgBackgroundProps = {
	holds: RouteHold[];
};

export default function SvgBackground(props: SvgBackgroundProps) {
	return (
		<svg className="h-full w-full">
			<defs>
				<mask id="backgroundMask">
					<rect x="0" y="0" width="100%" height="100%" fill="white" />
					{props.holds.map((hold: RouteHold) => {
						if (hold.type == 'volume') {
							return (
								<SvgPolygon
									key={hold.id}
									points={hold.polygon}
									holdRole={hold.role}
									color={'black'}
									fill={'black'}
								/>
							);
						} else {
							return (
								<SvgBezierPath
									key={hold.id}
									points={hold.polygon}
									holdRole={hold.role}
									color={'black'}
									fill={'black'}
								/>
							);
						}
					})}
				</mask>
			</defs>
			<rect
				className="h-full w-full"
				fill="gray"
				opacity="0.5"
				mask="url(#backgroundMask)"
			/>
		</svg>
	);
}
