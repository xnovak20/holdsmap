'use client';

import { Coordinates } from '@/types/coordinates-type';
import { MouseEventHandler, SVGProps } from 'react';
import { HoldRole } from '@/types/hold-role-type';

type SvgVolumeProps = Omit<SVGProps<SVGPolygonElement>, 'points'> & {
	points: Coordinates[];
	color: string;
	holdRole?: HoldRole;
	onClick?: MouseEventHandler<SVGPolygonElement>;
};

export default function SvgVolume({
	points,
	holdRole,
	color,
	...rest
}: SvgVolumeProps) {
	const polygonPoints: string = points
		.map((point) => `${point.x},${point.y}`)
		.join(' ');

	return (
		<polygon
			points={polygonPoints}
			fill={color}
			stroke={color}
			strokeWidth={3}
			strokeLinejoin={'round'}
			strokeLinecap={'round'}
			{...rest}
		/>
	);
}
