'use client';

import { SyntheticEvent, useState } from 'react';
import Image from 'next/image';

type WallImageProps = { image: string };
export default function WallImage(props: WallImageProps) {
	const [width, setWidth] = useState<number>(1000);
	const [height, setHeight] = useState<number>(1000);

	const handleImageLoad = (
		event: SyntheticEvent<HTMLImageElement, Event>
	) => {
		setWidth(event.currentTarget.naturalWidth);
		setHeight(event.currentTarget.naturalHeight);
	};

	return (
		<Image
			src={props.image}
			width={width}
			height={height}
			className="h-full w-full"
			alt="Picture of a wall full of holds."
			onLoad={handleImageLoad}
		/>
	);
}
