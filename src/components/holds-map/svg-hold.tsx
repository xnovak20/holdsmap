'use client';

import { Coordinates } from '@/types/coordinates-type';
import { MouseEventHandler, SVGProps } from 'react';
import { HoldRole } from '@/types/hold-role-type';

type SvgHoldProps = Omit<SVGProps<SVGPolygonElement>, 'points'> & {
	points: Coordinates[];
	color: string;
	holdRole?: HoldRole;
	onClick?: MouseEventHandler<SVGPolygonElement>;
};

export default function SvgHold({
	points,
	holdRole,
	color,
	...rest
}: SvgHoldProps) {
	const smoothing: number = 0.2;

	type Line = {
		angle: number;
		length: number;
	};
	const line = (pointA: Coordinates, pointB: Coordinates): Line => {
		const lengthX: number = pointB.x - pointA.x;
		const lengthY: number = pointB.y - pointA.y;
		return {
			length: Math.sqrt(Math.pow(lengthX, 2) + Math.pow(lengthY, 2)),
			angle: Math.atan2(lengthY, lengthX),
		};
	};

	const controlPoint = (
		currentPoint: Coordinates,
		previousPoint: Coordinates,
		nextPoint: Coordinates,
		endControlPoint: boolean = false
	): Coordinates => {
		const opposedLine: Line = line(previousPoint, nextPoint);

		const angle: number =
			opposedLine.angle + (endControlPoint ? Math.PI : 0);
		const length: number = opposedLine.length * smoothing;

		const x: number = currentPoint.x + Math.cos(angle) * length;
		const y: number = currentPoint.y + Math.sin(angle) * length;
		return { x, y };
	};

	const adjustPreviousPoint = (
		previousPoint: Coordinates,
		currentPoint: Coordinates,
		points: Coordinates[]
	) => {
		if (previousPoint !== undefined) {
			return previousPoint;
		} else {
			if (points.length >= 2) {
				return points[points.length - 2];
			} else {
				return currentPoint;
			}
		}
	};

	const adjustNextPoint = (
		nextPoint: Coordinates,
		currentPoint: Coordinates,
		points: Coordinates[]
	) => {
		if (nextPoint !== undefined) {
			return nextPoint;
		} else {
			if (points.length >= 2) {
				return points[1];
			} else {
				return currentPoint;
			}
		}
	};

	const bezierCommand = (
		point: Coordinates,
		pointIndex: number,
		points: Coordinates[]
	): string => {
		const startCurrentPoint: Coordinates = points[pointIndex - 1];
		const startPreviousPoint: Coordinates = adjustPreviousPoint(
			points[pointIndex - 2],
			startCurrentPoint,
			points
		);
		const startNextPoint: Coordinates = adjustNextPoint(
			point,
			startCurrentPoint,
			points
		);

		const startControlPoints: Coordinates = controlPoint(
			startCurrentPoint,
			startPreviousPoint,
			startNextPoint
		);

		const endCurrentPoint = point;
		const endPreviousPoint = adjustPreviousPoint(
			points[pointIndex - 1],
			endCurrentPoint,
			points
		);
		const endNextPoint = adjustNextPoint(
			points[pointIndex + 1],
			endCurrentPoint,
			points
		);

		const endControlPoint: Coordinates = controlPoint(
			endCurrentPoint,
			endPreviousPoint,
			endNextPoint,
			true
		);

		return `C ${startControlPoints.x},${startControlPoints.y} ${endControlPoint.x},${endControlPoint.y} ${point.x},${point.y}`;
	};

	// build the d attribute
	const getPathString = (points: Coordinates[]): string => {
		return points.reduce(
			(
				accumulator: string,
				currentPoint: Coordinates,
				pointIndex: number,
				points: Coordinates[]
			) => {
				if (pointIndex === 0) {
					return `M ${currentPoint.x},${currentPoint.y}`;
				} else {
					return `${accumulator} ${bezierCommand(currentPoint, pointIndex, points)}`;
				}
			},
			''
		);
	};

	const dParam: string = getPathString(points);

	return (
		<path
			d={dParam}
			fill={color}
			stroke={color}
			strokeWidth={3}
			strokeLinejoin="round"
			strokeLinecap="round"
			{...rest}
		/>
	);
}
