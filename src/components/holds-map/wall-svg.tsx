'use client';

import { v4 as uuidv4 } from 'uuid';
import { EditorMode } from '@/types/editor-mode-type';
import { Hold, RouteHold } from '@/types/hold-type';
import { MouseEventHandler, useEffect, useState } from 'react';
import { Coordinates } from '@/types/coordinates-type';
import { HoldRole } from '@/types/hold-role-type';
import {
	HoldToDraw,
	prepareToDraw,
	scaleHold,
} from '@/components/holds-map/holdsUtils';
import SvgVolume from '@/components/holds-map/svg-volume';
import SvgHold from '@/components/holds-map/svg-hold';
import StartCircle from '@/components/holds-map/svg-start-circle';

type WallSvgProps = {
	mode: EditorMode;
	parentWidth: number;
	parentHeight: number;
	wallId: string;
	holds: Hold[];
	routeHolds: RouteHold[];
	onChangeRoute: (updatedRouteHolds: RouteHold[]) => void;
	onChangeHolds: (updatedHolds: Hold[]) => void;
};

export default function WallSvg(props: WallSvgProps) {
	const [newHold, setNewHold] = useState<Hold>({
		id: uuidv4(),
		polygon: [],
		type: 'hold',
		wallId: props.wallId,
		createdAt: new Date(),
		deletedAt: null,
	});
	const [rerenderKey, setRerenderKey] = useState<number>(0);

	const forceSvgRerender = () => setRerenderKey(rerenderKey + 1);

	useEffect(() => {
		if (props.mode === 'createHolds') {
			setNewHold({ ...newHold, type: 'hold' });
			forceSvgRerender();
		} else if (props.mode === 'createVolumes') {
			setNewHold({ ...newHold, type: 'volume' });
			forceSvgRerender();
		}
	}, [props.mode]);

	const handleLeftClick = (
		event: React.MouseEvent<SVGSVGElement, MouseEvent>
	) => {
		const x: number =
			event.nativeEvent.offsetX / event.currentTarget.clientWidth;
		const y: number =
			event.nativeEvent.offsetY / event.currentTarget.clientHeight;
		console.log(x, y);
		if (props.mode === 'createHolds' || props.mode === 'createVolumes') {
			const newPoint: Coordinates = { x: x, y: y };
			setNewHold({ ...newHold, polygon: [...newHold.polygon, newPoint] });
		}
		forceSvgRerender();
	};

	const handleCircleClick: MouseEventHandler<SVGCircleElement> = (event) => {
		event.stopPropagation();
		if (props.mode === 'createHolds' || props.mode === 'createVolumes') {
			let hold: Hold = newHold;
			hold.polygon.push(newHold.polygon[0]);
			props.onChangeHolds([...props.holds, hold]);
			setNewHold({
				id: uuidv4(),
				polygon: [],
				type: 'hold',
				wallId: props.wallId,
				createdAt: new Date(),
				deletedAt: null,
			});
		}
		forceSvgRerender();
	};
	const removeRouteHold = (hold: RouteHold) => {
		props.onChangeRoute(
			props.routeHolds.filter((routeHold) => routeHold.id !== hold.id)
		);
	};

	const removeWallHold = (holdId: string) => {
		props.onChangeHolds(
			props.holds.filter((wallHold) => wallHold.id !== holdId)
		);
		forceSvgRerender();
	};

	const addRouteHold = (hold: Hold) => {
		const updatedRouteHolds: RouteHold[] = props.routeHolds;
		updatedRouteHolds.push({ ...hold, role: 'hold' });
		props.onChangeRoute(updatedRouteHolds);
	};

	const setRouteHoldRole = (hold: RouteHold, role: HoldRole) => {
		const updatedRoute = props.routeHolds;
		updatedRoute.forEach((routeHold: RouteHold) => {
			if (hold.id === routeHold.id) {
				routeHold.role = role;
			}
		});
		props.onChangeRoute(updatedRoute);
	};

	const handleRightClick = (event: React.MouseEvent, holdId: string) => {
		event.preventDefault();
		if (props.mode === 'createHolds') {
			removeWallHold(holdId);
		}
	};

	const handleHoldClick = (_event: React.MouseEvent, holdId: string) => {
		const startHoldCount: number = props.routeHolds.filter(
			(holdHold: RouteHold) => holdHold.role == 'start'
		).length;
		const topExists = props.routeHolds.some((hold) => hold.role === 'top');

		const holdInCurrentRoute: RouteHold | undefined = props.routeHolds.find(
			(routeHold) => routeHold.id === holdId
		);
		const holdInMap: Hold | undefined = props.holds.find(
			(routeHold) => routeHold.id === holdId
		);

		if (holdInCurrentRoute !== undefined) {
			if (props.mode === 'selectHolds') {
				if (holdInCurrentRoute.role === 'hold') {
					setRouteHoldRole(holdInCurrentRoute, 'foothold');
				} else if (
					holdInCurrentRoute.role === 'foothold' ||
					holdInCurrentRoute.role === 'start' ||
					holdInCurrentRoute.role === 'top'
				) {
					removeRouteHold(holdInCurrentRoute);
				}
			} else if (props.mode === 'selectStart') {
				if (holdInCurrentRoute.role === 'start') {
					setRouteHoldRole(holdInCurrentRoute, 'hold');
				} else if (startHoldCount < 2) {
					setRouteHoldRole(holdInCurrentRoute, 'start');
				}
			} else if (props.mode === 'selectTop') {
				if (holdInCurrentRoute.role == 'top') {
					setRouteHoldRole(holdInCurrentRoute, 'hold');
				} else if (holdInCurrentRoute.role === 'hold' && !topExists) {
					setRouteHoldRole(holdInCurrentRoute, 'top');
				}
			}
		} else if (holdInMap !== undefined) {
			if (props.mode === 'selectHolds') {
				addRouteHold(holdInMap);
			}
		} else {
			throw new Error(
				'Impossible. Hold must be in map, or something is very bad.'
			);
		}
		forceSvgRerender();
	};

	return (
		<svg
			key={rerenderKey}
			className="bg-gray h-full w-full bg-opacity-30"
			onClick={handleLeftClick}
		>
			{prepareToDraw(
				props.holds,
				props.routeHolds,
				props.mode,
				'volume',
				props.parentWidth,
				props.parentHeight
			).map((hold: HoldToDraw) => {
				return (
					<SvgVolume
						key={hold.id}
						points={hold.polygon}
						color={hold.color}
						fillOpacity={0.1}
						onClick={(event) => handleHoldClick(event, hold.id)}
						onContextMenu={(event) =>
							handleRightClick(event, hold.id)
						}
					/>
				);
			})}
			;
			{prepareToDraw(
				props.holds,
				props.routeHolds,
				props.mode,
				'hold',
				props.parentWidth,
				props.parentHeight
			).map((hold: HoldToDraw) => {
				return (
					<SvgHold
						key={hold.id}
						points={hold.polygon}
						color={hold.color}
						fillOpacity={0.1}
						onClick={(event) => handleHoldClick(event, hold.id)}
						onContextMenu={(event) =>
							handleRightClick(event, hold.id)
						}
					/>
				);
			})}
			;
			{newHold.type === 'hold' ? (
				<SvgHold
					fillOpacity={0.2}
					color={'white'}
					points={scaleHold(
						newHold.polygon,
						props.parentWidth,
						props.parentHeight
					)}
				/>
			) : (
				<SvgVolume
					fillOpacity={0.2}
					points={scaleHold(
						newHold.polygon,
						props.parentWidth,
						props.parentHeight
					)}
					color={'white'}
				/>
			)}
			{newHold.polygon.length > 0 && (
				<StartCircle
					position={
						scaleHold(
							newHold.polygon,
							props.parentWidth,
							props.parentHeight
						)[0]
					}
					onClick={handleCircleClick}
				/>
			)}
		</svg>
	);
}
