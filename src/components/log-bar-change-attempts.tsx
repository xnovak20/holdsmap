import { Button } from '@/components/ui/button';
import Remove from '@/components/images/remove';
import Add from '@/components/images/add-image';
import { useContext } from 'react';
import { FormContext, FormContextType } from '@/components/log-bar-modal';

export const ChangeAttempts = () => {
	const { form, setForm } = useContext(FormContext) as FormContextType;
	const changeValue = (direction: string) => {
		switch (direction) {
			case 'increase':
				setForm((prevState) => ({
					...prevState,
					attempts: form.attempts + 1,
				}));
				break;
			case 'decrease':
				setForm((prevState) => ({
					...prevState,
					attempts: form.attempts - 1,
				}));
				break;
		}
	};

	return (
		<span className=" rounded-2xl border pb-2 pt-2">
			<span className="flex items-center justify-center">
				<Button
					className="bg-transparent text-sky-500"
					onClick={() => changeValue('decrease')}
				>
					<Remove />
				</Button>
				{form.attempts} attempts
				<Button
					className="bg-transparent text-sky-500"
					onClick={() => changeValue('increase')}
				>
					<Add />
				</Button>
			</span>
		</span>
	);
};
