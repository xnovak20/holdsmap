'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import { FormProvider, useForm } from 'react-hook-form';
import { z } from 'zod';
import { Button } from '@/components/ui/button';
import { loginAction } from '@/actions/auth/login-action';
import { SignInWithGoogle } from '@/components/auth/sign-in-with-google';
import { toast } from 'sonner';
import Link from 'next/link';
import { FormInput } from '@/components/ui/form-controls/form-input';

const formSchema = z.object({
	email: z.string().email(),
	password: z.string().min(4),
});

export type LoginFormSchema = z.infer<typeof formSchema>;

export const LoginForm = () => {
	const form = useForm<LoginFormSchema>({
		resolver: zodResolver(formSchema),
	});

	const handleCredentialsLogin = async (values: LoginFormSchema) => {
		await loginAction('credentials', values).catch(() =>
			toast.error('Invalid credentials.')
		);
	};

	return (
		<FormProvider {...form}>
			<form
				onSubmit={form.handleSubmit(handleCredentialsLogin)}
				className="space-y-8"
			>
				<FormInput label="Email" name="email" />
				<FormInput
					label="Password"
					name="password"
					type="password"
					autoComplete="current-password"
				/>

				<div className="flex flex-col gap-4">
					<Button type="submit" variant="primary">
						Login
					</Button>
					<SignInWithGoogle />
					<div className="text-center text-sm">
						Don&apos;t have an account?{' '}
						<Link
							href="/auth/register"
							className="text-primary underline"
						>
							Register here
						</Link>
						.
					</div>
				</div>
			</form>
		</FormProvider>
	);
};

export default LoginForm;
