import { FC, PropsWithChildren } from 'react';

export const Title: FC<PropsWithChildren> = ({ children }) => (
	<h1 className="text-center text-3xl font-bold text-primary">{children}</h1>
);
