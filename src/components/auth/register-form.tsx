'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import { FormProvider, useForm } from 'react-hook-form';
import { z } from 'zod';
import { Button } from '@/components/ui/button';
import { SignInWithGoogle } from '@/components/auth/sign-in-with-google';
import { toast } from 'sonner';
import Link from 'next/link';
import { registerAction } from '@/actions/auth/register-action';
import { FormInput } from '@/components/ui/form-controls/form-input';

const formSchema = z.object({
	name: z.string().min(1, { message: 'First Name is required' }),
	surname: z.string().min(1, { message: 'Surname is required' }),
	email: z.string().email(),
	password: z.string().min(4),
});

export type RegisterFormSchema = z.infer<typeof formSchema>;

export const RegisterForm = () => {
	const form = useForm<RegisterFormSchema>({
		resolver: zodResolver(formSchema),
	});

	const handleRegister = async (values: RegisterFormSchema) => {
		await registerAction(values).catch((error: Error) =>
			toast.error(error.message)
		);
	};

	return (
		<FormProvider {...form}>
			<form
				onSubmit={form.handleSubmit(handleRegister)}
				className="space-y-8"
			>
				<FormInput label="Name" name="name" />
				<FormInput label="Surname" name="surname" />
				<FormInput label="Email" name="email" autoComplete="username" />
				<FormInput
					label="Password"
					name="password"
					type="password"
					autoComplete="new-password"
				/>

				<div className="flex flex-col gap-4">
					<Button type="submit" variant="primary">
						Create an account
					</Button>

					<SignInWithGoogle />

					<div className="text-center text-sm">
						Already have an account?{' '}
						<Link
							href="/auth/login"
							className="text-primary underline"
						>
							Log in
						</Link>
					</div>
				</div>
			</form>
		</FormProvider>
	);
};

export default RegisterForm;
