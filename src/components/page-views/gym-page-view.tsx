import React, { FC, PropsWithChildren, ReactNode } from 'react';
import { LinkProps } from 'next/link';
import { TopBar } from '@/components/top-bar';

type Props = {
	title: string;
	subtitle?: string;
	backButtonPath?: LinkProps['href'];
	actions?: ReactNode;
} & PropsWithChildren;

export const GymPageView: FC<Props> = ({
	title,
	subtitle,
	backButtonPath,
	actions,
	children,
}) => (
	<>
		<TopBar
			header={title}
			subheader={subtitle}
			backButtonPath={backButtonPath}
		>
			{actions}
		</TopBar>
		{children}
	</>
);
