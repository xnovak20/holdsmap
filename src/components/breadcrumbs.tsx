import { FC, Fragment } from 'react';
import Link from 'next/link';

type Breadcrumb = {
	title: string;
	href?: string;
};

type Props = {
	items: Breadcrumb[];
};

export const Breadcrumbs: FC<Props> = ({ items }) => {
	return (
		<div className="flex gap-1 pb-4 text-sm">
			<BreadcrumbDivider />
			{items.map((item, index) => (
				<Fragment key={index}>
					{index !== 0 && <BreadcrumbDivider />}
					{!item.href ? (
						<div className="select-none text-gray-400">
							{item.title}
						</div>
					) : (
						<Link href={item.href}>{item.title}</Link>
					)}
				</Fragment>
			))}
		</div>
	);
};

const BreadcrumbDivider = () => <span className="text-gray-200">/</span>;
