import { useFormState } from 'react-hook-form';
import { FC } from 'react';

type Props = {
	name: string;
};

export const FormError: FC<Props> = ({ name }) => {
	const { errors } = useFormState();

	return (
		!!errors[name] && (
			<span className="mt-1 text-sm text-red-600">
				{errors[name]?.message?.toString()}
			</span>
		)
	);
};
