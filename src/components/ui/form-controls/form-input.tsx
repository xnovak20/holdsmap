import React, { FC } from 'react';

import { Label } from '@/components/ui/form-controls/primitives/label';
import { Input } from '@/components/ui/form-controls/primitives/input';
import { FormFieldProps, MultiClassName } from '@/types/Base';
import { cn, cnKey } from '@/lib/utils';
import { useFormContext } from 'react-hook-form';
import { FormError } from '@/components/ui/form-controls/form-error';

type Props = {
	type?: React.HTMLInputTypeAttribute | undefined;
} & FormFieldProps &
	MultiClassName<'wrapper' | 'label' | 'input'>;

export const FormInput: FC<Props> = ({ type, label, name, id, className }) => {
	const { register } = useFormContext();

	id ??= name;

	return (
		<div className={cn(cnKey(className, 'wrapper'))}>
			<Label
				htmlFor={id}
				className={cn('block pb-2 pl-2', cnKey(className, 'label'))}
			>
				{label}
			</Label>
			<Input
				type={type}
				id={id}
				className={cn(
					'h-auto rounded-xl px-4 py-3 ring-primary transition-all hover:bg-gray-50 focus-visible:bg-gray-50 focus-visible:ring focus-visible:ring-offset-0',
					cnKey(className, 'input', true)
				)}
				{...register(name)}
			/>
			<FormError name={name} />
		</div>
	);
};
