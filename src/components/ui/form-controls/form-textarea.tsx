import React, { FC } from 'react';

import { Label } from '@/components/ui/form-controls/primitives/label';
import { FormFieldProps, MultiClassName } from '@/types/Base';
import { cn, cnKey } from '@/lib/utils';
import { Textarea } from '@/components/ui/form-controls/primitives/textarea';
import { FormError } from '@/components/ui/form-controls/form-error';
import { useFormContext } from 'react-hook-form';

type Props = FormFieldProps & MultiClassName<'wrapper' | 'label' | 'input'>;

export const FormTextarea: FC<Props> = ({ label, name, id, className }) => {
	const { register } = useFormContext();

	id ??= name;

	return (
		<div className={cn(cnKey(className, 'wrapper'))}>
			<Label htmlFor={id} className={cn(cnKey(className, 'label'))}>
				{label}
			</Label>
			<Textarea
				id={id}
				className={cn(
					'h-auto rounded-xl px-4 py-3 ring-primary transition-all hover:bg-gray-50 focus-visible:bg-gray-50 focus-visible:ring focus-visible:ring-offset-0',
					cnKey(className, 'input', true)
				)}
				{...register(name)}
			/>
			<FormError name={name} />
		</div>
	);
};
