'use client';

import { FC, ReactNode, useCallback } from 'react';

import { Label } from '@/components/ui/form-controls/primitives/label';
import { FormFieldProps, MultiClassName } from '@/types/Base';
import { cn, cnKey } from '@/lib/utils';
import { Checkbox } from '@/components/ui/form-controls/primitives/checkbox';
import { FormError } from '@/components/ui/form-controls/form-error';
import { CheckedState } from '@radix-ui/react-checkbox';
import { Controller } from 'react-hook-form';

type Props = {
	label:
		| ReactNode
		| {
				on: ReactNode;
				off: ReactNode;
		  };
} & Omit<FormFieldProps, 'label' | 'value' | 'defaultValue'> &
	MultiClassName<'wrapper' | 'label' | 'input'>;

export const FormCheckbox: FC<Props> = ({ label, name, id, className }) => {
	id ??= name;

	const generateLabel = useCallback((checked: CheckedState) => {
		if (
			typeof label === 'object' &&
			label &&
			'on' in label &&
			'off' in label
		) {
			return checked ? label.on : label.off;
		}

		return label;
	}, []);

	return (
		<Controller
			name={name}
			render={({ field: { value, onChange } }) => (
				<div
					className={cn(
						'flex items-center space-x-2',
						cnKey(className, 'wrapper')
					)}
				>
					<Checkbox
						id={id}
						name={name}
						className={cn(
							'data-[state=checked]:border-primary data-[state=checked]:bg-primary',
							cnKey(className, 'input', true)
						)}
						checked={value}
						onCheckedChange={onChange}
					/>
					<Label
						htmlFor={id}
						className={cn(cnKey(className, 'label'))}
					>
						{generateLabel(value)}
					</Label>
					<FormError name={name} />
				</div>
			)}
		/>
	);
};
