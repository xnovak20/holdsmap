import * as React from 'react';
import { Slot } from '@radix-ui/react-slot';
import { cva, type VariantProps } from 'class-variance-authority';

import { cn } from '@/lib/utils';

const buttonVariants = cva(
	'rounded-2xl inline-flex items-center justify-center whitespace-nowrap text-sm font-medium ring-offset-white transition-all duration-300 focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-gray-950 focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50',
	{
		variants: {
			variant: {
				default: '',
				primary: 'bg-primary hover:bg-primary-hover text-white',
				secondary: 'bg-secondary text-white hover:bg-secondary-hover',
				gradient:
					'bg-gradient-to-r from-primary to-secondary text-white',
				destructive:
					'bg-destructive hover:bg-destructive-hover text-white',
				outline: 'border-2 border-gray-200 hover:border-primary',
				ghost: 'hover:bg-gray-100 hover:text-gray-900',
				link: 'text-gray-900 underline-offset-4 hover:underline',
				filter: 'bg-transparent text-primary ring ring-primary',
				redirect:
					'rounded-xl bg-gradient-to-r from-cyan-500 to-blue-500 rounded-2xl border text-white',
				invisible: 'bg-transparent border-none',
			},
			size: {
				default: 'px-4 py-3',
				sm: 'px-4 py-3',
				lg: 'px-4 py-3',
				icon: 'px-4 py-3',
			},
		},
		defaultVariants: {
			variant: 'default',
			size: 'default',
		},
	}
);

export interface ButtonProps
	extends React.ButtonHTMLAttributes<HTMLButtonElement>,
		VariantProps<typeof buttonVariants> {
	asChild?: boolean;
}

const Button = React.forwardRef<HTMLButtonElement, ButtonProps>(
	({ className, variant, size, asChild = false, ...props }, ref) => {
		const Comp = asChild ? Slot : 'button';
		return (
			<Comp
				className={cn(buttonVariants({ variant, size, className }))}
				ref={ref}
				{...props}
			/>
		);
	}
);
Button.displayName = 'Button';

export { Button, buttonVariants };
