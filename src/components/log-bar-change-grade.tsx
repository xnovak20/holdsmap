import { useContext } from 'react';
import { Button } from '@/components/ui/button';
import Remove from '@/components/images/remove';
import Add from '@/components/images/add-image';
import {
	MAX_GRADE,
	MIN_GRADE,
} from '@/app/gyms/[gymId]/climbs/_components/filter-utils';
import { ScaleName } from '@/types/Grade/scaleName';
import { FormContext, FormContextType } from './log-bar-modal';

const ChangeGrade = () => {
	const { form, setForm } = useContext(FormContext) as FormContextType;
	const changeValue = (direction: string) => {
		if (form.grade.getValue() === MAX_GRADE && direction === 'increase')
			return;
		if (form.grade.getValue() === MIN_GRADE && direction === 'decrease')
			return;

		switch (direction) {
			case 'increase':
				form.grade.setFromValue(form.grade.getValue() + 1);
				break;
			case 'decrease':
				form.grade.setFromValue(form.grade.getValue() - 1);
				break;
		}

		setForm((prevState) => ({
			...prevState,
			grade: form.grade,
		}));
	};

	return (
		<span className="rounded-2xl border  pb-2 pt-2">
			<span className="flex items-center justify-center">
				Grade opinion
			</span>
			<span className="flex items-center justify-center">
				<Button
					className="bg-transparent text-sky-500"
					onClick={() => changeValue('decrease')}
				>
					<Remove />
				</Button>
				<span className="text-sky-500">
					{form.grade.toString(ScaleName.Font)}
				</span>
				<Button
					className="bg-transparent text-sky-500"
					onClick={() => changeValue('increase')}
				>
					<Add />
				</Button>
			</span>
		</span>
	);
};

export default ChangeGrade;
