'use client';

import { TransformComponent, TransformWrapper } from 'react-zoom-pan-pinch';
import React, { useEffect, useState } from 'react';

type FullWidthScrollableWrapperProps = {
	children?: React.ReactNode;
};

export const FullWidthScrollableWrapper = (
	props: FullWidthScrollableWrapperProps
) => {
	const [windowWidth, setWindowWidth] = useState(window.innerWidth);

	useEffect(() => {
		const handleResize = () => {
			setWindowWidth(window.innerWidth);
		};

		window.addEventListener('resize', handleResize);

		return () => {
			window.removeEventListener('resize', handleResize);
		};
	}, []);

	//todo lepe nastylovat, aby tam nebylu scrollbary a zabiralo to celou spodni cast stranky
	// todo cca nejak takto: https://bettertyped.github.io/react-zoom-pan-pinch/?path=/docs/examples-responsive-image--responsive-image
	return (
		<TransformWrapper>
			<TransformComponent contentStyle={{ width: windowWidth }}>
				{props.children}
			</TransformComponent>
		</TransformWrapper>
	);
};
