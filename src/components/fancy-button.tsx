import Link from 'next/link';
import { Button } from '@/components/ui/button';
import React, { ComponentProps, ComponentType, FC } from 'react';
import { cn } from '@/lib/utils';

type Props = {
	title: string;
	href?: string;
	icon: ComponentType<ComponentProps<any>>;
	className?: string;
};

export const FancyButton: FC<Props> = ({
	title,
	href,
	icon: IconComponent,
	className,
}) => {
	const children = (
		<>
			<IconComponent className="absolute right-0 size-48 -rotate-12 opacity-20 drop-shadow transition-transform duration-300 group-hover:scale-110" />
			{title}
		</>
	);

	return (
		<Button
			asChild
			className={cn(
				'group relative inline-flex w-full justify-start overflow-hidden px-8 py-10 text-xl font-semibold transition-shadow hover:shadow-md',
				className
			)}
		>
			{href ? (
				<Link href={href}>{children}</Link>
			) : (
				<div className="cursor-pointer">{children}</div>
			)}
		</Button>
	);
};
