import React from 'react';

export const Loader = () => (
	<svg
		xmlns="http://www.w3.org/2000/svg"
		width="24"
		height="24"
		viewBox="0 0 24 24"
		fill="none"
		stroke="currentColor"
		strokeWidth="2"
		strokeLinecap="round"
		strokeLinejoin="round"
		className="size-32 animate-[move_35s_linear_infinite]"
	>
		<circle cx="6" cy="19" r="3" />
		<path
			d="M9 19h8.5a3.5 3.5 0 0 0 0-7h-11a3.5 3.5 0 0 1 0-7H15"
			className="[stroke-dasharray:15]"
		/>
		<circle cx="18" cy="5" r="3" />
	</svg>
);
