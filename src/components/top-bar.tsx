import { ArrowLeft } from 'lucide-react';
import Link, { LinkProps } from 'next/link';
import { PropsWithChildren, ReactNode } from 'react';
import { cn } from '@/lib/utils';
import { Logout } from '@/components/logout';

type TopBarProps = {
	children?: ReactNode;
	header: string;
	subheader?: string;
	backButtonPath?: LinkProps['href'];
} & PropsWithChildren;

export const TopBar = ({
	backButtonPath,
	children,
	header,
	subheader,
}: TopBarProps) => {
	return (
		<div className="sticky top-0 z-40 flex h-24 items-center bg-primary">
			<div className="container flex items-center justify-between">
				<div className="flex items-center gap-2">
					{!!backButtonPath && (
						<div className="bg-opacity-0">
							<Link
								href={backButtonPath}
								className="rounded-full"
							>
								<ArrowLeft className="size-8 text-white transition-transform hover:scale-105" />
							</Link>
						</div>
					)}
					<div
						className={cn(
							'flex flex-col justify-center text-black',
							!!backButtonPath && 'ml-6'
						)}
					>
						<h1 className={cn('text-xl text-white')}>{header}</h1>
						{!!subheader && (
							<h2 className="text-white">{subheader}</h2>
						)}
					</div>
				</div>
				<div className="flex items-center gap-2">{children}</div>
			</div>
		</div>
	);
};
