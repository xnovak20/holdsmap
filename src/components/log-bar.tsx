'use client';
import React from 'react';
import { Button } from '@/components/ui/button';
import { Minus, Plus } from 'lucide-react';
import { DescriptionModal } from '@/components/log-bar-modal-description';
import { LogBarModal } from './log-bar-modal';
import { updateAscentAction } from '@/actions/update-ascent-action';

type LogBarProps = {
	attempts: number;
	sent: boolean;
	routeDescription: string;
	userId: string;
	routeId: string;
};
const LogBar = (props: LogBarProps) => {
	return (
		<>
			<div className="flex justify-around">
				<DescriptionModal routeDescription={props.routeDescription} />
				<Button
					disabled={props.sent}
					onClick={async () => {
						await updateAscentAction(
							props.routeId,
							props.userId,
							props.attempts - 1
						);
					}}
				>
					<Minus />
				</Button>
				<div>
					{(props.sent ? 'Sent in ' : 'You did ') +
						props.attempts +
						' attempts'}
				</div>

				<Button
					disabled={props.sent}
					onClick={async () => {
						await updateAscentAction(
							props.routeId,
							props.userId,
							props.attempts + 1
						);
					}}
				>
					<Plus />
				</Button>
				<LogBarModal
					disabled={props.sent}
					attempts={props.attempts}
					userId={props.userId}
					routeDescription={props.routeDescription}
					routeId={props.routeId}
				/>
			</div>
		</>
	);
};

export default LogBar;
