'use client';
import { useRouter } from 'next/navigation';
import React from 'react';
import { Button } from '@/components/ui/button';

type RedirectButtonProps = {
	children?: React.ReactNode;
	path: string;
	className?: string;
	disabled?: boolean; // New optional prop
};

// TODO: Delete this, use Button component instead
// 	 		 <Button asChild><Link href="URL_TO_REDIRECT">XX</Link></Button>
export const RedirectButton = (props: RedirectButtonProps) => {
	const router = useRouter();

	const handleClick = () => {
		if (!props.disabled) {
			router.push(props.path);
		}
	};

	return (
		<Button
			className={props.className}
			onClick={handleClick}
			disabled={props.disabled}
			variant="primary"
		>
			{props.children}
		</Button>
	);
};
