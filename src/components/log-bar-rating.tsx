import Star from './images/star';
import { useContext } from 'react';
import { FormContext, FormContextType } from '@/components/log-bar-modal';
import { Button } from './ui/button';

export const ChangeRating = () => {
	const { form, setForm } = useContext(FormContext) as FormContextType;

	const handleClick = (stars: number) => {
		setForm((prevState) => ({
			...prevState,
			rating: stars,
		}));
	};

	return (
		<div
			className={
				'flex flex-col items-center justify-center rounded-2xl border pb-4 pt-2'
			}
		>
			<span>Is this route delightful?</span>
			<div className={'flex items-center justify-center'}>
				<Button
					onClick={() => handleClick(1)}
					className={'px-1'}
					variant={'invisible'}
				>
					<Star fill={form.rating > 0} />
				</Button>
				<Button
					onClick={() => handleClick(2)}
					className={'px-1'}
					variant={'invisible'}
				>
					<Star fill={form.rating > 1} />
				</Button>
				<Button
					onClick={() => handleClick(3)}
					className={'px-1'}
					variant={'invisible'}
				>
					<Star fill={form.rating > 2} />
				</Button>
				<Button
					onClick={() => handleClick(4)}
					className={'px-1'}
					variant={'invisible'}
				>
					<Star fill={form.rating > 3} />
				</Button>
				<Button
					onClick={() => handleClick(5)}
					className={'px-1'}
					variant={'invisible'}
				>
					<Star fill={form.rating > 4} />
				</Button>
			</div>
		</div>
	);
};
