'use client';

import { FC, ReactNode, useState } from 'react';
import {
	Dialog,
	DialogContent,
	DialogHeader,
	DialogTitle,
	DialogTrigger,
} from '@/components/ui/dialog';

type Props = {
	trigger: ReactNode;
};

export const PremiumAccountModal: FC<Props> = ({ trigger }) => {
	const [open, setOpen] = useState(false);

	return (
		<Dialog open={open} onOpenChange={setOpen}>
			<DialogTrigger asChild>{trigger}</DialogTrigger>
			<DialogContent>
				<DialogHeader>
					<DialogTitle>Premium feature</DialogTitle>
				</DialogHeader>
				This feature requires premium account.
				<img src="/xdd.webp" alt="xdd" className="mx-auto" />
			</DialogContent>
		</Dialog>
	);
};
