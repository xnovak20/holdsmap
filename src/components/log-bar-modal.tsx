'use client';

import {
	Dialog,
	DialogContent,
	DialogFooter,
	DialogHeader,
	DialogTitle,
	DialogTrigger,
} from '@/components/ui/dialog';
import { createContext, Dispatch, SetStateAction, useState } from 'react';
import { Button } from '@/components/ui/button';
import { Check } from 'lucide-react';
import ChangeGrade from './log-bar-change-grade';
import { Grade } from '@/types/Grade/Grade';
import { ChangeAttempts } from './log-bar-change-attempts';
import { ChangeRating } from '@/components/log-bar-rating';
import { logSendAction } from '@/actions/log-send-action';

type FormType = {
	attempts: number;
	grade: Grade;
	rating: number;
};

export type FormContextType = {
	form: FormType;
	setForm: Dispatch<SetStateAction<FormType>>;
};

export const FormContext = createContext<FormContextType | null>(null);

type ModalProps = {
	disabled: boolean;
	attempts: number;
	routeDescription: string;
	userId: string;
	routeId: string;
};
export const LogBarModal = (props: ModalProps) => {
	const [isModalOpened, setIsModalOpened] = useState(false);
	const [form, setForm] = useState<FormType>({
		attempts: props.attempts + 1,
		grade: new Grade({ value: 6 }),
		rating: 3,
	});

	const handleSave = async () => {
		await logSendAction(
			props.routeId,
			props.userId,
			form.attempts,
			form.grade.getValue(),
			form.rating
		);
		setIsModalOpened(false);
	};

	const handleOnOpenChange = (open: boolean) => {
		setForm((prevForm) => ({
			...prevForm,
			attempts: props.attempts + 1,
		}));
		setIsModalOpened(open);
	};

	return (
		<FormContext.Provider value={{ form, setForm }}>
			<Dialog open={isModalOpened} onOpenChange={handleOnOpenChange}>
				<DialogTrigger asChild>
					<Button disabled={props.disabled} variant={'redirect'}>
						<Check />
					</Button>
				</DialogTrigger>
				<DialogContent>
					<DialogHeader>
						<DialogTitle>Confirm send</DialogTitle>
					</DialogHeader>
					<ChangeAttempts />
					<ChangeGrade />
					<ChangeRating />
					<DialogFooter>
						<Button
							disabled={form.attempts < 1}
							onClick={handleSave}
							className="rounded-2xl bg-sky-500"
							variant={'redirect'}
							type="submit"
						>
							Send
						</Button>
					</DialogFooter>
				</DialogContent>
			</Dialog>
		</FormContext.Provider>
	);
};
