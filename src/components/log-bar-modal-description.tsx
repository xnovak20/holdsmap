'use client';

import {
	Dialog,
	DialogContent,
	DialogDescription,
	DialogHeader,
	DialogTitle,
	DialogTrigger,
} from '@/components/ui/dialog';
import { Button } from '@/components/ui/button';
import { Info } from 'lucide-react';

export const DescriptionModal = ({
	routeDescription,
}: {
	routeDescription: string;
}) => {
	return (
		<Dialog>
			<DialogTrigger asChild>
				<Button variant={'redirect'}>
					<Info />
				</Button>
			</DialogTrigger>
			<DialogContent>
				<DialogHeader>
					<DialogTitle>Description</DialogTitle>
					<DialogDescription>{routeDescription}</DialogDescription>
				</DialogHeader>
			</DialogContent>
		</Dialog>
	);
};
