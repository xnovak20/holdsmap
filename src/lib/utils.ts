import { type ClassValue, clsx } from 'clsx';
import { twMerge } from 'tailwind-merge';
import { MultiClassName } from '@/types/Base';

export function cn(...inputs: ClassValue[]) {
	return twMerge(clsx(inputs));
}

export const cnKey = <TIdentifier extends string>(
	className:
		| MultiClassName<TIdentifier>['className']
		| string
		| object
		| undefined,
	key: TIdentifier,
	isDefaultClassName = false
) => {
	if (typeof className === 'string') {
		return isDefaultClassName ? className : undefined;
	}

	return className?.[key as keyof typeof className] ?? isDefaultClassName;
};
