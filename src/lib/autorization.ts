import { User } from '@/db/schema/user';

class Authorization {
	private _user: User | null = null;

	public user(user: User) {
		this._user = user;

		return this;
	}

	public hasAccessTo<TModel extends object, TColumn extends keyof TModel>(
		model: TModel,
		accessorColumn: TColumn
	): boolean {
		return model[accessorColumn] === this._user?.id;
	}

	constructor(user: User | null) {
		this._user = user;
	}
}

export function authorize(user: User) {
	return new Authorization(user);
}
