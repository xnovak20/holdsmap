import { v4 as uuidv4 } from 'uuid';
import { db } from '@/db';
import { route } from '@/db/schema/route';
import { wall } from '@/db/schema/wall';
import { and, count, eq, isNull } from 'drizzle-orm';
import holdsRepository from './hold-repository';
import holdToRouteRepository from '@/repository/hold-to-route-repository';
import { FetchedRoute, InsertRoute, Route } from '@/types/route-type';
import { RouteHold } from '@/types/hold-type';

class RouteRepository {
	//CREATE
	async createRoute(
		name: string,
		description: string,
		grade: number,
		wallId: string,
		authorId: string,
		holds: RouteHold[]
	): Promise<string> {
		const newRoute: InsertRoute = {
			id: uuidv4(),
			createdAt: new Date(),
			grade: grade,
			description: description,
			name: name,
			wallId: wallId,
			userId: authorId,
			deletedAt: null,
		};
		await db.insert(route).values({ ...newRoute });

		for (const hold of holds) {
			await holdToRouteRepository.addHoldToRoute(
				hold.id,
				newRoute.id,
				hold.role
			);
		}
		return newRoute.id;
	}

	// READ
	async getRouteById(routeId: string): Promise<Route | null> {
		const fetchedRoutes: FetchedRoute[] = await db
			.select()
			.from(route)
			.where(and(eq(route.id, routeId), isNull(route.deletedAt)));
		if (fetchedRoutes.length === 1) {
			const routeHolds: RouteHold[] =
				await holdsRepository.getRouteHolds(routeId);
			return { ...fetchedRoutes[0], holds: routeHolds };
		}
		return null;
	}

	async getRoutesByGymId(gymId: string): Promise<FetchedRoute[]> {
		return db
			.select({
				id: route.id,
				name: route.name,
				description: route.description,
				grade: route.grade,
				createdAt: route.createdAt,
				deletedAt: route.deletedAt,
				wallId: route.wallId,
				userId: route.userId,
			})
			.from(route)
			.leftJoin(wall, eq(wall.id, route.wallId))
			.where(and(eq(wall.gymId, gymId), isNull(route.deletedAt)));
	}

	async getRouteCountByGymId(gymId: string): Promise<number> {
		const result: { count: number }[] = await db
			.select({ count: count() })
			.from(route)
			.leftJoin(wall, eq(wall.id, route.wallId))
			.where(and(eq(wall.gymId, gymId), isNull(route.deletedAt)));

		return result.length === 1 ? result[0].count : 0;
	}

	async all() {
		return db.select().from(route).where(isNull(route.deletedAt));
	}

	//UPDATE
	//todo

	//DELETE
	async deleteRoute(routeId: string) {
		await db
			.update(route)
			.set({ deletedAt: new Date() })
			.where(eq(route.id, routeId));
	}

	async restoreRoute(routeId: string) {
		await db
			.update(route)
			.set({ deletedAt: null })
			.where(eq(route.id, routeId));
	}
}

const routeRepository = new RouteRepository();

export default routeRepository;
