import { and, eq, InferInsertModel, isNull, like, ne, or } from 'drizzle-orm';
import { User, user } from '@/db/schema/user';
import { db } from '@/db';
import { v4 as uuidv4 } from 'uuid';
import { gym } from '@/db/schema/gym';
import { gymToUserBookmark } from '@/db/schema/gym-to-user-bookmark';
import { FetchedUser } from '@/types/user-type';

class UserRepository {
	async all() {
		return db.select().from(user);
	}

	async getById(id: string) {
		return db.select().from(user).where(eq(user.id, id)).get();
	}

	async getByEmail(email: string): Promise<FetchedUser | undefined> {
		return db.select().from(user).where(eq(user.email, email)).get();
	}

	async matchCredentials({
		email,
		password,
	}: {
		email: string;
		password: string;
	}) {
		return db
			.select()
			.from(user)
			.where(and(eq(user.email, email), eq(user.password, password)))
			.get();
	}

	async create(
		newUser: Omit<
			InferInsertModel<typeof user>,
			'createdAt' | 'deletedAt' | 'id'
		>
	): Promise<boolean> {
		return (
			(
				await db
					.insert(user)
					.values({ id: uuidv4(), createdAt: new Date(), ...newUser })
					.returning()
			).length > 0
		);
	}

	use(user: User) {
		return {
			getGyms: async (q?: string) =>
				await db.query.gym.findMany({
					where: (gyms, { eq, and, or, like }) =>
						and(
							isNull(gyms.deletedAt),
							eq(gyms.ownerId, user.id),
							or(
								like(gym.name, `%${q}%`),
								like(gym.location, `%${q}%`)
							)
						),
				}),

			getBookmarkedGyms: async (q?: string) =>
				await db
					.select({
						id: gym.id,
						name: gym.name,
						private: gym.private,
						location: gym.location,
						description: gym.description,
						news: gym.news,
						ownerId: gym.ownerId,
						createdAt: gym.createdAt,
						deletedAt: gym.deletedAt,
					})
					.from(gym)
					.leftJoin(
						gymToUserBookmark,
						eq(gymToUserBookmark.gymId, gym.id)
					)
					.where(
						and(
							isNull(gym.deletedAt),
							eq(gymToUserBookmark.userId, user.id),
							q
								? or(
										like(gym.name, `%${q}%`),
										like(gym.location, `%${q}%`)
									)
								: undefined
						)
					),

			getNotBookmarkedGyms: async (q?: string) =>
				await db
					.select({
						id: gym.id,
						name: gym.name,
						private: gym.private,
						location: gym.location,
						description: gym.description,
						news: gym.news,
						ownerId: gym.ownerId,
						createdAt: gym.createdAt,
						deletedAt: gym.deletedAt,
					})
					.from(gym)
					.leftJoin(
						gymToUserBookmark,
						eq(gymToUserBookmark.gymId, gym.id)
					)
					.where(
						and(
							isNull(gym.deletedAt),
							or(
								ne(gymToUserBookmark.userId, user.id),
								isNull(gymToUserBookmark.userId)
							),
							q
								? or(
										like(gym.name, `%${q}%`),
										like(gym.location, `%${q}%`)
									)
								: undefined
						)
					),
		};
	}

	async getAuthorById(userId: string): Promise<FetchedUser | null> {
		const data: FetchedUser[] = await db
			.select()
			.from(user)
			.where(eq(user.id, userId));
		if (data.length !== 1) {
			return null;
		} else {
			return data[0];
		}
	}
}

const userRepository = new UserRepository();

export default userRepository;
