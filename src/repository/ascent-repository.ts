import { FetchedAscent } from '@/types/ascent-type';
import { db } from '@/db';
import { and, eq } from 'drizzle-orm';
import { v4 as uuidv4 } from 'uuid';
import { ascent } from '@/db/schema/ascent';

class AscentRepository {
	async getAscentByRouteIdAndUserId(
		routeId: string,
		userId: string
	): Promise<FetchedAscent | null> {
		try {
			const fetchedAscent: FetchedAscent[] = await db
				.select()
				.from(ascent)
				.where(
					and(eq(ascent.userId, userId), eq(ascent.routeId, routeId))
				);

			if (fetchedAscent.length !== 1) {
				return null;
			}
			return fetchedAscent[0];
		} catch (e) {
			console.error(e);
			return null;
		}
	}

	async updateAttempts(
		routeId: string,
		userId: string,
		attemptCount: number
	): Promise<number | null> {
		try {
			const currentAscent: FetchedAscent | null =
				await this.getAscentByRouteIdAndUserId(routeId, userId);
			if (currentAscent == null) {
				await db.insert(ascent).values({
					attempts: attemptCount,
					id: uuidv4(),
					routeId: routeId,
					userId: userId,
				});
			} else {
				await db
					.update(ascent)
					.set({ attempts: attemptCount })
					.where(
						and(
							eq(ascent.userId, userId),
							eq(ascent.routeId, routeId)
						)
					);
			}
		} catch (e) {
			console.error(e);
			return null;
		}
		return attemptCount;
	}

	async logSend(
		routeId: string,
		userId: string,
		attemptCount: number,
		stars: number,
		gradeOpinion: number
	): Promise<number | null> {
		try {
			const currentAscent: FetchedAscent | null =
				await this.getAscentByRouteIdAndUserId(routeId, userId);
			if (currentAscent == null) {
				await db.insert(ascent).values({
					attempts: attemptCount,
					id: uuidv4(),
					routeId: routeId,
					userId: userId,
					gradeOpinion: gradeOpinion,
					sentAt: new Date(),
					stars: stars,
				});
			} else {
				await db
					.update(ascent)
					.set({
						attempts: attemptCount,
						gradeOpinion: gradeOpinion,
						sentAt: new Date(),
						stars: stars,
					})
					.where(
						and(
							eq(ascent.userId, userId),
							eq(ascent.routeId, routeId)
						)
					);
			}
		} catch (e) {
			console.error(e);
			return null;
		}
		return attemptCount;
	}

	async all() {
		return db.select().from(ascent);
	}
}

const ascentRepository = new AscentRepository();
export default ascentRepository;
