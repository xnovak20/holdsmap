import { v4 as uuidv4 } from 'uuid';
import { hold } from '@/db/schema/hold';
import { eq } from 'drizzle-orm';
import { HoldType } from '@/types/hold-type-type';
import { Point } from '@/types/point-type';
import { FetchedHold, Hold, InsertHold, RouteHold } from '@/types/hold-type';
import pointRepository from '@/repository/point-repository';
import holdToRouteRepository from '@/repository/hold-to-route-repository';
import { db } from '@/db';

class HoldRepository {
	//CREATE
	async createHold(wallId: string, holdType: HoldType, polygon: Point[]) {
		const newHold: InsertHold = {
			id: uuidv4(),
			type: holdType,
			createdAt: new Date(),
			wallId: wallId,
		};
		await db.insert(hold).values(newHold);

		for (const { x, y } of polygon) {
			await pointRepository.createPoint(x, y, newHold.id);
		}
		return newHold;
	}

	//READ
	async getHoldsFromWall(wallId: string): Promise<Hold[]> {
		const fetchedHolds: FetchedHold[] = await db
			.select()
			.from(hold)
			.where(eq(hold.wallId, wallId));

		return await Promise.all(
			fetchedHolds.map(async (hold) => {
				const polygon = await pointRepository.getPointsByHoldId(
					hold.id
				);
				return { ...hold, polygon };
			})
		);
	}

	async getHoldById(holdId: string): Promise<Hold | null> {
		const fetchedHolds: FetchedHold[] = await db
			.select()
			.from(hold)
			.where(eq(hold.id, holdId));

		if (fetchedHolds.length === 1) {
			const polygon = await pointRepository.getPointsByHoldId(
				fetchedHolds[0].id
			);
			return { ...fetchedHolds[0], polygon };
		}
		return null;
	}

	async getRouteHolds(routeId: string): Promise<RouteHold[]> {
		const routeToHoldRelation =
			await holdToRouteRepository.getHoldsByRouteId(routeId);

		const routeHolds: RouteHold[] = [];
		for (const relation of routeToHoldRelation) {
			const hold: Hold | null = await this.getHoldById(relation.holdId);
			if (hold !== null) {
				routeHolds.push({ ...hold, role: relation.role });
			}
		}
		return routeHolds;
	}

	//UPDATE
	async editHold(holdId: string, holdType?: HoldType, polygon?: Point[]) {
		if (polygon != null) {
			await db.transaction(async (_tx) => {
				await pointRepository.deletePointsByHoldId(holdId);
				for (const { x, y } of polygon) {
					await pointRepository.createPoint(x, y, holdId);
				}
			});
		}
		if (holdType != null) {
			await db
				.update(hold)
				.set({ type: holdType })
				.where(eq(hold.id, holdId));
		}
	}

	//DELETE
	async deleteHold(h: string | object) {
		if (typeof h === 'string') {
			await pointRepository.deletePointsByHoldId(h);
			await db.delete(hold).where(eq(hold.id, h));
		} else if ('id' in h && typeof h.id === 'string') {
			await pointRepository.deletePointsByHoldId(h.id);
			await db.delete(hold).where(eq(hold.id, h.id));
		} else {
			throw new Error(
				'To deleteHold method must be passed hold objet with id.'
			);
		}
	}
	async deleteHolds(holds: string[] | object[]) {
		for (const h of holds) {
			await this.deleteHold(h);
		}
	}

	async deleteHoldsByWallId(wallId: string) {
		const wallHolds: FetchedHold[] = await db
			.select()
			.from(hold)
			.where(eq(hold.wallId, wallId));

		await this.deleteHolds(wallHolds);
	}
}

const holdsRepository = new HoldRepository();

export default holdsRepository;
