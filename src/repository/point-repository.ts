import { v4 as uuidv4 } from 'uuid';
import { eq } from 'drizzle-orm';
import { FetchedPoint } from '@/types/point-type';
import { point } from '@/db/schema/point';
import { db } from '@/db';

class PointRepository {
	//CREATE
	async createPoint(
		x: number,
		y: number,
		holdId: string
	): Promise<FetchedPoint> {
		if (holdId == null) {
			throw new Error('Cannot create point to undefined holdId.');
		}
		const newPoint: typeof point.$inferInsert = {
			id: uuidv4(),
			x: x,
			y: y,
			holdId: holdId,
		};
		await db.insert(point).values(newPoint);
		return newPoint;
	}

	//READ
	async getPointById(pointId: string): Promise<FetchedPoint | null> {
		const result = await db
			.select()
			.from(point)
			.where(eq(point.id, pointId));
		return result.length === 1 ? result[0] : null;
	}

	async getPointsByHoldId(holdId: string): Promise<FetchedPoint[]> {
		return db.select().from(point).where(eq(point.holdId, holdId));
	}

	//DELETE
	async deletePoint(pointId: string): Promise<void> {
		await db.delete(point).where(eq(point.id, pointId));
	}

	async deletePoints(pointIds: string[]): Promise<void> {
		for (const pointId of pointIds) {
			await this.deletePoint(pointId);
		}
	}

	async deletePointsByHoldId(holdId: string): Promise<void> {
		await db.delete(point).where(eq(point.holdId, holdId));
	}
}

const pointRepository = new PointRepository();

export default pointRepository;
