import { db } from '@/db';
import { and, eq, isNull } from 'drizzle-orm';
import { wall } from '@/db/schema/wall';
import { FetchedWall } from '@/types/wall-type';
import { v4 as uuidv4 } from 'uuid';

class WallRepository {
	async all() {
		return db.select().from(wall);
	}

	async update(
		id: string,
		data: Omit<
			Partial<FetchedWall>,
			'id' | 'createdAt' | 'deletedAt' | 'gymId'
		>
	) {
		await db.update(wall).set(data).where(eq(wall.id, id));
	}

	async create(data: Omit<FetchedWall, 'id' | 'createdAt' | 'deletedAt'>) {
		await db.insert(wall).values({
			id: uuidv4(),
			...data,
			createdAt: new Date(),
		});
	}

	async getAllWallsByGymId(gymId: string): Promise<FetchedWall[]> {
		return db
			.select()
			.from(wall)
			.where(and(isNull(wall.deletedAt), eq(wall.gymId, gymId)));
	}

	async getWallById(wallId: string): Promise<FetchedWall | null> {
		const fetchedWalls: FetchedWall[] = await db
			.select()
			.from(wall)
			.where(and(isNull(wall.deletedAt), eq(wall.id, wallId)));

		return fetchedWalls.length === 1 ? fetchedWalls[0] : null;
	}

	async getWallsByGymId(gymId: string): Promise<FetchedWall[]> {
		return db
			.select()
			.from(wall)
			.where(and(isNull(wall.deletedAt), eq(wall.gymId, gymId)));
	}
}

const wallRepository = new WallRepository();

export default wallRepository;
