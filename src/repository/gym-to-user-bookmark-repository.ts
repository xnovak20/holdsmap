import { db } from '@/db';
import { gymToUserBookmark } from '@/db/schema/gym-to-user-bookmark';
import { and, eq } from 'drizzle-orm';

class GymToUserBookmarkRepository {
	async getGymBookmark(gymId: string, userId: string) {
		return db
			.select()
			.from(gymToUserBookmark)
			.where(
				and(
					eq(gymToUserBookmark.gymId, gymId),
					eq(gymToUserBookmark.userId, userId)
				)
			);
	}

	async addGymBookmark(gymId: string, userId: string) {
		return db
			.insert(gymToUserBookmark)
			.values({ userId: userId, gymId: gymId });
	}

	async removeGymBookmark(gymId: string, userId: string) {
		return db
			.delete(gymToUserBookmark)
			.where(
				and(
					eq(gymToUserBookmark.gymId, gymId),
					eq(gymToUserBookmark.userId, userId)
				)
			);
	}
}

const gymToUserBookmarkRepository = new GymToUserBookmarkRepository();
export default gymToUserBookmarkRepository;
