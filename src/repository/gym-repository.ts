import { db } from '@/db';
import { gym } from '@/db/schema/gym';
import { gymToUserBookmark } from '@/db/schema/gym-to-user-bookmark';
import { and, eq, isNull, like, ne, or } from 'drizzle-orm';
import { FetchedGym } from '@/types/gym-type';
import { User } from '@/db/schema/user';

class GymRepository {
	async getAllBookmarkedGymsByUser(userId: string, searchPrompt?: string) {
		return db
			.select({
				id: gym.id,
				name: gym.name,
				private: gym.private,
				location: gym.location,
				description: gym.description,
				news: gym.news,
				createdAt: gym.createdAt,
				deletedAt: gym.deletedAt,
			})
			.from(gym)
			.leftJoin(gymToUserBookmark, eq(gymToUserBookmark.gymId, gym.id))
			.where(
				and(
					isNull(gym.deletedAt),
					eq(gymToUserBookmark.userId, userId),
					or(
						like(gym.name, `%${searchPrompt}%`),
						like(gym.location, `%${searchPrompt}%`)
					)
				)
			);
	}

	async getAllNonBookmarkedGymsByUser(userId: string, searchPrompt?: string) {
		return db
			.select({
				id: gym.id,
				name: gym.name,
				private: gym.private,
				location: gym.location,
				description: gym.description,
				news: gym.news,
				createdAt: gym.createdAt,
				deletedAt: gym.deletedAt,
			})
			.from(gym)
			.leftJoin(gymToUserBookmark, eq(gymToUserBookmark.gymId, gym.id))
			.where(
				and(
					isNull(gym.deletedAt),
					or(
						ne(gymToUserBookmark.userId, userId),
						isNull(gymToUserBookmark.userId)
					),
					or(
						like(gym.name, `%${searchPrompt}%`),
						like(gym.location, `%${searchPrompt}%`)
					)
				)
			);
	}

	async getGymById(gymId: string): Promise<FetchedGym | null> {
		const result: FetchedGym[] = await db
			.select()
			.from(gym)
			.where(eq(gym.id, gymId));

		return result.length === 1 ? result[0] : null;
	}

	async all({ q, user }: { q?: string; relations?: object; user?: User }) {
		return db.query.gym.findMany({
			with: {
				gymToUserBookmark: true,
			},
			where: (gyms, { and, or, isNull, like }) =>
				and(
					isNull(gyms.deletedAt),
					q
						? or(
								like(gym.name, `%${q}%`),
								like(gym.location, `%${q}%`)
							)
						: undefined
				),
		});
	}
}

const gymRepository = new GymRepository();

export default gymRepository;
