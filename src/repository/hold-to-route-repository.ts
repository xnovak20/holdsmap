import { HoldRole } from '@/types/hold-role-type';
import {
	FetchedHoldToRoute,
	InsertHoldToRoute,
} from '@/types/hold-to-route-type';
import { db } from '@/db';
import { holdToRoute } from '@/db/schema/hold-to-route';
import { and, eq } from 'drizzle-orm';

class HoldToRouteRepository {
	//CREATE
	async addHoldToRoute(
		holdId: string,
		routeId: string,
		role: HoldRole
	): Promise<FetchedHoldToRoute> {
		const newHoldToRoute: InsertHoldToRoute = {
			holdId: holdId,
			routeId: routeId,
			role: role,
		};
		await db.insert(holdToRoute).values(newHoldToRoute);

		return newHoldToRoute;
	}

	//READ
	async getHoldsByRouteId(routeId: string): Promise<FetchedHoldToRoute[]> {
		return db
			.select()
			.from(holdToRoute)
			.where(eq(holdToRoute.routeId, routeId));
	}

	async getByHoldId(holdId: string): Promise<FetchedHoldToRoute[]> {
		return db
			.select()
			.from(holdToRoute)
			.where(eq(holdToRoute.holdId, holdId));
	}

	//UPDATE
	async updateHoldRole(
		holdId: string,
		routeId: string,
		role: HoldRole
	): Promise<void> {
		await db
			.update(holdToRoute)
			.set({ role: role })
			.where(
				and(
					eq(holdToRoute.holdId, holdId),
					eq(holdToRoute.routeId, routeId)
				)
			);
	}
	//DELETE
	async removeHoldFromRoute(holdId: string, routeId: string): Promise<void> {
		await db
			.delete(holdToRoute)
			.where(
				and(
					eq(holdToRoute.holdId, holdId),
					eq(holdToRoute.routeId, routeId)
				)
			);
	}

	async removeHoldsFromRoute(routeId: string): Promise<void> {
		await db.delete(holdToRoute).where(eq(holdToRoute.routeId, routeId));
	}
}

const holdToRouteRepository = new HoldToRouteRepository();

export default holdToRouteRepository;
