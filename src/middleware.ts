import { auth } from '@/auth';
import app from '@/config/app';

export default auth((req) => {
	if (!req.auth) {
		const url = req.nextUrl.clone();

		url.pathname = app.notAuthenticatedRedirectPath;

		return Response.redirect(url);
	}
});

export const config = {
	matcher: '/((?!api|auth|_next/static|_next/image|favicon.ico).*)',
};
