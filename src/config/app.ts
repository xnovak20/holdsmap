type AppConfig = {
	homePath: string;
	notAuthenticatedRedirectPath: string;

	fileMaxSize: number;
	fileImageTypes: string[];
};

const app = {
	/*
	 |------------------------------------------------------------
	 | Variables for authentication
	 |------------------------------------------------------------
	 |
	*/

	homePath: '/',
	notAuthenticatedRedirectPath: '/auth/login',

	/*
   |------------------------------------------------------------
   | Variables for file uploading
   |------------------------------------------------------------
   |
  */

	fileMaxSize: 5000000,
	fileImageTypes: [
		'image/jpeg',
		'image/jpg',
		'image/png',
		'image/webp',
		'image/svg+xml',
	],
} satisfies AppConfig;

export default app;
