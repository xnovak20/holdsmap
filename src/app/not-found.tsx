import { Metadata } from 'next';
import React from 'react';

export const metadata: Metadata = {
	title: '404',
};

export default function Page() {
	const errorTitles = [
		'Lost in the Ether',
		'The Black Hole of the Internet',
		'Error 404: Page Vanished into Thin Air',
		'Welcome to the Void',
		'404: Page Not Found... in This Dimension',
		'Digital Wilderness Ahead',
		"Oops! Looks Like You've Gone Off the Grid",
		'Into the Abyss: Page Not Detected',
		'404: Where Did the Page Go?',
		'Page Lost in Cyberspace',
	];
	const randomIndex = Math.floor(Math.random() * errorTitles.length);

	return (
		<div
			className="flex min-h-screen flex-col items-center justify-center bg-gradient-to-t from-cyan-500 to-blue-500
			 pl-4 pr-4 text-white"
		>
			<h1 className="bg-gradient-to-r from-purple-400 to-pink-600 bg-clip-text text-8xl font-extrabold text-transparent contrast-125">
				404!
			</h1>
			<h2 className="text-md bg-clip-text font-extrabold text-white contrast-125">
				{errorTitles[randomIndex]}
			</h2>
		</div>
	);
}
