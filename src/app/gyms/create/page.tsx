import { GymCreateForm } from '@/components/gym/gym-create-form';
import { GymPageView } from '@/components/page-views/gym-page-view';
import { Breadcrumbs } from '@/components/breadcrumbs';
import React from 'react';

const CreateGymPage = () => {
	return (
		<GymPageView title="Create gym" backButtonPath="/gyms">
			<Breadcrumbs
				items={[{ title: 'Gyms', href: '/gyms' }, { title: 'Create' }]}
			/>

			<div className="container max-w-lg">
				<GymCreateForm />
			</div>
		</GymPageView>
	);
};

export default CreateGymPage;
