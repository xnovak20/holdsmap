import {
	BrickWallIcon,
	MedalIcon,
	NewspaperIcon,
	PencilLine,
	RouteIcon,
} from 'lucide-react';
import { FetchedGym } from '@/types/gym-type';
import React from 'react';
import gymRepository from '@/repository/gym-repository';
import { getAuthenticatedUser } from '@/auth';
import { notFound } from 'next/navigation';
import { GymPageView } from '@/components/page-views/gym-page-view';
import Link from 'next/link';
import { authorize } from '@/lib/autorization';
import { Breadcrumbs } from '@/components/breadcrumbs';
import { PremiumAccountModal } from '@/components/modals/premium-account-modal';
import { FancyButton } from '@/components/fancy-button';

type GymDetailPageProps = {
	params: {
		gymId: string;
	};
};

const GymDetailPage = async (props: GymDetailPageProps) => {
	const gym: FetchedGym | null = await gymRepository.getGymById(
		props.params.gymId
	);
	const user = await getAuthenticatedUser();

	if (!gym) {
		notFound();
	}

	return (
		<GymPageView
			title={gym.name}
			subtitle={
				gym.private ? 'Private gym' : gym.location ?? 'Unknown location'
			}
			backButtonPath="/gyms"
			actions={
				authorize(user).hasAccessTo(gym, 'ownerId') ? (
					<Link href={`/gyms/${gym.id}/edit`}>
						<PencilLine className="size-10 text-white" />
					</Link>
				) : null
			}
		>
			<Breadcrumbs
				items={[{ title: 'Gyms', href: '/gyms' }, { title: 'Gym' }]}
			/>

			<div className="space-y-6">
				{!!gym.news && (
					<div className="flex items-start justify-start gap-4 rounded-xl bg-black p-4 font-mono text-white">
						<div className="mt-1">
							<NewspaperIcon className="size-6" />
						</div>
						<div className="w-full">
							<h3 className="text-md mb-2 font-semibold">News</h3>
							<div className="text-sm">{gym.news}</div>
						</div>
					</div>
				)}

				{!!gym.description && <div>{gym.description}</div>}

				<div className="grid grid-cols-1 gap-4 md:grid-cols-3">
					<FancyButton
						href={`/gyms/${gym.id}/walls`}
						icon={BrickWallIcon}
						title="Walls"
						className="bg-secondary text-white"
					/>
					<FancyButton
						href={`/gyms/${gym.id}/climbs`}
						icon={RouteIcon}
						title="Routes"
						className="bg-primary text-white"
					/>
					<PremiumAccountModal
						trigger={
							<FancyButton
								icon={MedalIcon}
								title="Leaderboard"
								className="bg-warning text-white"
							/>
						}
					/>
				</div>
			</div>
		</GymPageView>
	);
};

export default GymDetailPage;
