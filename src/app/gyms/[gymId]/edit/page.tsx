import { getAuthenticatedUser } from '@/auth';
import { GymEditForm } from '@/components/gym/gym-edit-form';
import { GymPageView } from '@/components/page-views/gym-page-view';
import gymRepository from '@/repository/gym-repository';
import { notFound } from 'next/navigation';
import { authorize } from '@/lib/autorization';
import { FetchedGym } from '@/types/gym-type';

const GymEditPage = async ({ params }: { params: { gymId: string } }) => {
	const gym: FetchedGym | null = await gymRepository.getGymById(params.gymId);
	const user = await getAuthenticatedUser();

	if (!gym) {
		notFound();
	}

	if (!authorize(user).hasAccessTo(gym, 'ownerId')) {
		throw new Error('Not authorized');
	}

	return (
		<GymPageView
			title="Edit gym"
			subtitle={gym?.name}
			backButtonPath={`/gyms/${gym.id}`}
		>
			<div className="container max-w-lg">
				<GymEditForm gym={gym} />
			</div>
		</GymPageView>
	);
};

export default GymEditPage;
