'use client';
import HoldsMap from '@/components/holds-map/holds-map';
import { FullWidthScrollableWrapper } from '@/components/full-width-scrollable-wrapper';
import { FetchedWall } from '@/types/wall-type';
import { Hold } from '@/types/hold-type';
import { updateWallHoldsAction } from '@/actions/update-wall-holds-action';

type EditWallHoldsPageProps = {
	gymId: string;
	wall: FetchedWall;
	wallHolds: Hold[];
};
export const CreateHoldsHoldMap = (props: EditWallHoldsPageProps) => {
	const handleHoldsChange = async (updatedHolds: Hold[]) => {
		await updateWallHoldsAction(props.wall.id, props.gymId, updatedHolds);
	};

	return (
		<FullWidthScrollableWrapper>
			<HoldsMap
				mode={'createHolds'}
				image={props.wall.picture}
				holds={props.wallHolds}
				routeHolds={[]}
				onChangeRoute={() => {}}
				onChangeHolds={handleHoldsChange}
				wallId={props.wall.id}
			/>
		</FullWidthScrollableWrapper>
	);
};
