import { TopBar } from '@/components/top-bar';
import { getWallHoldsAction } from '@/actions/get-wall-holds-action';
import { getWallByIdAction } from '@/actions/get-wall-by-id-action';
import { notFound } from 'next/navigation';
import { Hold } from '@/types/hold-type';
import { FetchedWall } from '@/types/wall-type';
import { CreateHoldsHoldMap } from '@/app/gyms/[gymId]/walls/[wallId]/edit/holds/_components/CreateHoldsHoldMap';

type EditWallHoldsPageProps = {
	params: {
		gymId: string;
		wallId: string;
	};
};
const EditWallHoldsPage = async (props: EditWallHoldsPageProps) => {
	const wallHolds: Hold[] = await getWallHoldsAction(props.params.wallId);
	const wall: FetchedWall | null = await getWallByIdAction(
		props.params.wallId
	);
	if (!wall) {
		notFound();
	}

	return (
		<>
			<TopBar
				header={'Create holds'}
				subheader={'Left to create, right to delete.'}
				backButtonPath={`/gyms/${props.params.gymId}/walls`}
			/>
			<div className="flex w-full items-center justify-center">
				<div className="centered-div">
					<CreateHoldsHoldMap
						wall={wall}
						gymId={props.params.gymId}
						wallHolds={wallHolds}
					/>
				</div>
			</div>
		</>
	);
};

export default EditWallHoldsPage;
