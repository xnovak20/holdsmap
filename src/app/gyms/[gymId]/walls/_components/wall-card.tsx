'use client';

import React from 'react';
import { formatDate } from '@/app/gyms/[gymId]/walls/_utils/format-date';
import { FetchedRoute } from '@/types/route-type';
import { FetchedWall } from '@/types/wall-type';
import { BarChart2 } from 'lucide-react';
import WallModal from '@/app/gyms/[gymId]/walls/_components/wall-modal';

type WallCardProps = {
	wall: FetchedWall;
	routes: FetchedRoute[];
	gymId: string;
};

const WallCard = ({ wall, routes, gymId }: WallCardProps) => {
	const numOfRoutes = routes.filter(
		(route) => route.wallId === wall.id
	).length;

	return (
		<div
			className="relative m-2 rounded-xl border border-gray-100 bg-cover bg-center bg-no-repeat p-2 pb-16 shadow transition-all hover:scale-105 hover:shadow-lg"
			style={{
				backgroundImage: `linear-gradient(to bottom, rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url(${wall.picture})`,
			}}
		>
			<div className="flex justify-between">
				<span className="pl-2 pt-2 text-3xl font-bold text-white drop-shadow-xl">
					<a href={`/gyms/${gymId}/climbs?wallId=${wall.id}`}>
						{wall.name}
					</a>
				</span>
				<WallModal gymId={gymId} wall={wall} modalMode={'edit'} />
			</div>
			<div className="flex justify-between">
				<span className="absolute bottom-0 left-0 flex items-end justify-end pb-2 pl-4 font-bold text-white drop-shadow-xl">
					{formatDate(wall.createdAt)}
				</span>
				<span className="absolute bottom-0 right-0 flex items-end justify-end pb-2 pr-2">
					<span className="pr-2 text-white">
						{numOfRoutes} {numOfRoutes !== 1 ? 'Routes' : 'Route'}
					</span>
					<BarChart2 className="h-12 w-12 text-white" />
				</span>
			</div>
		</div>
	);
};

export default WallCard;
