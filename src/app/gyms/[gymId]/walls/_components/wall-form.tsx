import React, { useState } from 'react';
import { Button } from '@/components/ui/button';
import { Input } from '@/components/ui/input';
import { Label } from '@/components/ui/label';
import { RedirectButton } from '@/components/redirect-button';
import { UploadDropzone } from '@/components/upload-thing/utils';
import Image from 'next/image';
import { toast } from 'sonner';
import { FetchedWall } from '@/types/wall-type';
import { ResultMessage } from '@/types/result-message';
import { DeleteWallAction } from '@/actions/delete-wall';
import { createWall, updateWall } from '@/app/gyms/[gymId]/walls/actions';
import { useParams } from 'next/navigation';

type ModalMode = 'add' | 'edit';

type WallFormProps = {
	wall?: FetchedWall;
	modalMode: ModalMode;
	gymId: string;
	setIsModalOpened: React.Dispatch<React.SetStateAction<boolean>>;
};

const WallForm = ({
	wall,
	modalMode,
	gymId,
	setIsModalOpened,
}: WallFormProps) => {
	const params = useParams();
	const [errorRequiredFields, setErrorRequiredFields] = useState(false);
	const [formName, setFormName] = useState(wall?.name ?? '');
	const [formPicture, setFormPicture] = useState(wall?.picture ?? '');

	const handleDelete = async () => {
		const response = await DeleteWallAction(wall?.id!);
		if ('success' in response) {
			toast.success(response.success);
			setErrorRequiredFields(false);
			setIsModalOpened(false);
		} else {
			toast.error(response.error);
		}
	};

	const handleSave = async () => {
		if (formName.length <= 3 || formPicture === '') {
			setErrorRequiredFields(true);
		} else {
			let response: ResultMessage;
			switch (modalMode) {
				case 'edit':
					response = await updateWall({
						id: wall?.id!,
						name: formName,
						picture: formPicture,
					});
					break;
				case 'add':
					response = await createWall({
						name: formName,
						picture: formPicture,
						gymId: gymId,
					});
					break;
			}

			if ('success' in response) {
				toast.success(response.success);
				if (modalMode === 'add') {
					setFormName('');
					setFormPicture('');
				}
				setErrorRequiredFields(false);
				setIsModalOpened(false);
			} else {
				toast.error(response.error);
			}
		}
	};

	const renderUploadDropzone = () => (
		<div className="w-1/2">
			<Label>Upload picture:</Label>
			<UploadDropzone
				endpoint="imageUploader"
				onClientUploadComplete={(res) => {
					setFormPicture(res[0].url);
					setErrorRequiredFields(false);
					toast.success(
						'Picture loaded successfully to ' + res[0].url
					);
				}}
				onUploadError={(error: Error) => {
					toast.error('Failed to upload picture: ' + error.message);
				}}
			/>
		</div>
	);

	const renderPreview = () =>
		formPicture && (
			<div className="w-1/2 space-y-2">
				<Label>Preview:</Label>
				<div className="relative h-64 w-full rounded-lg border-2 border-black">
					<Image
						src={formPicture}
						alt={'Picture of ' + formName}
						layout="fill"
						objectFit="cover"
					/>
				</div>
			</div>
		);

	return (
		<div className={'flex flex-col gap-4'}>
			<Label>Name</Label>
			<Input
				defaultValue={formName}
				onChange={(e) => setFormName(e.target.value)}
				className="col-span-3"
			/>
			<div className="flex gap-4">
				{renderUploadDropzone()}
				{renderPreview()}
			</div>
			{errorRequiredFields && (
				<span className="text-sm text-red-600">
					All fields are required. Wall name must have at least 3
					characters.
				</span>
			)}
			{modalMode === 'edit' && (
				<>
					<RedirectButton
						className="bg-secondary"
						path={`/gyms/${params.gymId}/walls/${wall?.id}/edit/holds`}
					>
						Edit holds
					</RedirectButton>
					<Button
						onClick={handleDelete}
						variant="primary"
						className="w-full bg-red-500 hover:bg-red-700"
					>
						Delete wall
					</Button>
				</>
			)}
			<Button
				onClick={handleSave}
				type="submit"
				variant="primary"
				className="w-full"
			>
				Save
			</Button>
		</div>
	);
};

export default WallForm;
