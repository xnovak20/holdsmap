'use client';

import React, { useState } from 'react';
import {
	Dialog,
	DialogContent,
	DialogHeader,
	DialogTitle,
	DialogTrigger,
} from '@/components/ui/dialog';
import { PencilLine, PlusIcon } from 'lucide-react';
import { FetchedWall } from '@/types/wall-type';
import WallForm from '@/app/gyms/[gymId]/walls/_components/wall-form';

type ModalMode = 'add' | 'edit';

type WallModalProps = {
	wall?: FetchedWall;
	modalMode: ModalMode;
	gymId: string;
};

const WallModal = ({ wall, modalMode, gymId }: WallModalProps) => {
	const [isModalOpened, setIsModalOpened] = useState(false);

	return (
		<Dialog open={isModalOpened} onOpenChange={setIsModalOpened}>
			<DialogTrigger asChild>
				{modalMode === 'edit' ? (
					<button className="small items-end p-2">
						<PencilLine className="h-10 w-10 text-white" />
					</button>
				) : (
					<button className="block size-auto items-center justify-center p-2 text-white hover:animate-spin">
						<PlusIcon className="size-10" />
					</button>
				)}
			</DialogTrigger>
			<DialogContent className="rounded-2xl sm:max-w-[600px]">
				<DialogHeader>
					<DialogTitle>
						{modalMode === 'edit' ? 'Edit Wall' : 'Add Wall'}
					</DialogTitle>
				</DialogHeader>
				<WallForm
					wall={wall}
					modalMode={modalMode}
					gymId={gymId}
					setIsModalOpened={setIsModalOpened}
				/>
			</DialogContent>
		</Dialog>
	);
};

export default WallModal;
