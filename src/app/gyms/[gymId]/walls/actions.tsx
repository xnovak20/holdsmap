'use server';

import wallRepository from '@/repository/wall-repository';
import { revalidatePath } from 'next/cache';
import { v4 as uuidv4 } from 'uuid';
import { writeFile } from 'fs/promises';
import path from 'path';
import { ResultMessage } from '@/types/result-message';

export const updateWall = async ({
	id,
	name,
	picture,
}: {
	id: string;
	name: string;
	picture: string;
}): Promise<ResultMessage> => {
	try {
		await wallRepository.update(id, { name, picture });

		revalidatePath('/gyms', 'layout');
		return { success: "'Wall updated successfully" };
	} catch (error) {
		return { error: `Failed to update wall: ${error}` };
	}
};

export const createWall = async ({
	name,
	picture,
	gymId,
}: {
	name: string;
	picture: string;
	gymId: string;
}) => {
	try {
		await wallRepository.create({ name, picture, gymId });
		revalidatePath('/gyms', 'layout');
		return { success: 'Wall added successfully.' };
	} catch (error) {
		return { success: `Unable to add wall: ${error}` };
	}
};

export const updateWallWithFileUpload = async ({
	wallId,
	formData,
}: {
	wallId: string;
	formData: FormData;
}) => {
	const name = formData.get('name') as string;
	const picture = formData.get('picture') as File;

	const uploadedFile = await uploadFile(picture);
	if ('error' in uploadedFile) {
		return uploadedFile;
	} else {
		await wallRepository.update(wallId, {
			name,
			picture: uploadedFile.uri,
		});
		revalidatePath('/walls');
		return { success: 'Wall updated successfully' };
	}
};

export const createWallWithFileUpload = async ({
	gymId,
	formData,
}: {
	gymId: string;
	formData: FormData;
}) => {
	const name = formData.get('name') as string;
	const picture = formData.get('picture') as File;

	const uploadedFile = await uploadFile(picture);
	if ('error' in uploadedFile) {
		return uploadedFile;
	} else {
		await wallRepository.create({
			name,
			picture: uploadedFile.uri,
			gymId: gymId,
		});
		revalidatePath('/walls');
		return { success: 'Wall created successfully' };
	}
};

type UploadFileResponse =
	| {
			fileName: string;
			uri: string;
			absolutePath: string;
	  }
	| { error: string };

const uploadFile = async (file: File): Promise<UploadFileResponse> => {
	if (!(file instanceof File)) {
		return { error: 'No file selected.' };
	}
	const buffer = Buffer.from(await file.arrayBuffer());

	const fileName = `wall_${uuidv4()}_${file.name.replaceAll(' ', '_')}`;
	const uri = `/storage/${fileName}`;
	const absolutePath = path.join(process.cwd(), `public/${uri}`);

	try {
		await writeFile(absolutePath, buffer);

		return {
			fileName,
			uri,
			absolutePath,
		};
	} catch (error) {
		return { error: 'Unable to upload a file' };
	}
};
