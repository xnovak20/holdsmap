import { TopBar } from '@/components/top-bar';
import { FetchedGym } from '@/types/gym-type';
import { FetchedWall } from '@/types/wall-type';
import WallCard from './_components/wall-card';
import { Breadcrumbs } from '@/components/breadcrumbs';
import React from 'react';
import gymRepository from '@/repository/gym-repository';
import routeRepository from '@/repository/route-repository';
import { FetchedRoute } from '@/types/route-type';
import wallRepository from '@/repository/wall-repository';
import WallModal from '@/app/gyms/[gymId]/walls/_components/wall-modal';

type WallsPageProps = {
	params: {
		gymId: string;
	};
};

export async function generateMetadata(props: WallsPageProps) {
	const gym: FetchedGym | null = await gymRepository.getGymById(
		props.params.gymId
	);

	return {
		title: `Walls | ${gym ? gym.name : 'Unknown gym'}`,
	};
}

const WallsPage = async (props: WallsPageProps) => {
	const routes: FetchedRoute[] = await routeRepository.all();
	const walls: FetchedWall[] = await wallRepository.getWallsByGymId(
		props.params.gymId
	);
	const gym: FetchedGym | null = await gymRepository.getGymById(
		props.params.gymId
	);

	return (
		<>
			<TopBar
				header={'Walls'}
				subheader={gym ? `in ${gym.name}` : ''}
				backButtonPath={`/gyms/${props.params.gymId}`}
			>
				<WallModal gymId={props.params.gymId} modalMode={'add'} />
			</TopBar>

			<div className="container py-8">
				<Breadcrumbs
					items={[
						{ title: 'Gyms', href: '/gyms' },
						{ title: 'Gym', href: `/gyms/${gym!.id}` },
						{ title: 'Walls' },
					]}
				/>

				<div className="w-full space-y-8 bg-white">
					<div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3">
						{walls.map((wall, index) => (
							<WallCard
								key={index}
								gymId={props.params.gymId}
								wall={wall}
								routes={routes}
							/>
						))}
					</div>
				</div>
			</div>
		</>
	);
};

export default WallsPage;
