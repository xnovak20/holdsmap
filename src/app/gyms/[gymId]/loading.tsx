import { Loader } from '@/components/loader';

const Loading = () => {
	return (
		<div className="flex h-screen animate-pulse items-center justify-center text-primary">
			<Loader />
		</div>
	);
};

export default Loading;
