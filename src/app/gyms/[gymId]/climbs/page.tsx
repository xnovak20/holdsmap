import { TopBar } from '@/components/top-bar';
import Link from 'next/link';
import { FetchedGym } from '@/types/gym-type';
import { PlusIcon } from 'lucide-react';
import Filter from './_components/filter';
import { getAuthenticatedUser } from '@/auth';
import React from 'react';
import ascentRepository from '@/repository/ascent-repository';
import { RouteAscents } from '@/app/gyms/[gymId]/climbs/_components/filter-utils';
import gymRepository from '@/repository/gym-repository';
import userRepository from '@/repository/user-repository';
import routeRepository from '@/repository/route-repository';
import WallRepository from '@/repository/wall-repository';
import { FetchedWall } from '@/types/wall-type';
import { FetchedRoute } from '@/types/route-type';
import { FetchedUser } from '@/types/user-type';
import { FetchedAscent } from '@/types/ascent-type';

type ClimbsPageProps = {
	params: {
		gymId: string;
	};
};

export async function generateMetadata(props: ClimbsPageProps) {
	const gym: FetchedGym | null = await gymRepository.getGymById(
		props.params.gymId
	);
	return {
		title: `Climbs | ${gym ? gym.name : 'Unknown gym'}`,
	};
}

const Page = async (props: ClimbsPageProps) => {
	const currentUser = await getAuthenticatedUser();
	const fetchedWalls: FetchedWall[] = await WallRepository.getWallsByGymId(
		props.params.gymId
	);
	const fetchedRoutes: FetchedRoute[] =
		await routeRepository.getRoutesByGymId(props.params.gymId);
	const fetchedUsers: FetchedUser[] = await userRepository.all();
	const fetchedAscents: FetchedAscent[] = await ascentRepository.all();

	const routeAscents: RouteAscents[] = fetchedRoutes.map((route) => {
		const ascents = fetchedAscents.filter(
			(ascent) => ascent.routeId === route.id
		); // Assuming there's a function to get ascents by route id
		return { routeRating: 0, route, ascents };
	});

	return (
		<>
			<TopBar
				header="Climbs"
				backButtonPath={`/gyms/${props.params.gymId}`}
				subheader={`Total of ${fetchedRoutes.length} climbs in this gym`}
			>
				<Link
					className="block size-auto items-center justify-center bg-transparent p-2 text-white hover:animate-spin"
					href={`/gyms/${props.params.gymId}/climbs/create/select-wall`}
				>
					<PlusIcon className="size-10 bg-transparent " />
				</Link>
			</TopBar>
			<div className="container pb-8">
				{fetchedRoutes && fetchedRoutes.length > 0 && (
					<Filter
						routeAscents={routeAscents}
						users={fetchedUsers}
						walls={fetchedWalls}
						currentUser={currentUser}
					/>
				)}
			</div>
		</>
	);
};

export default Page;
