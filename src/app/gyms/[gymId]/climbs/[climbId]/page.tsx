import { TopBar } from '@/components/top-bar';
import HoldsMap from '@/components/holds-map/holds-map';
import { notFound } from 'next/navigation';
import { FullWidthScrollableWrapper } from '@/components/full-width-scrollable-wrapper';
import { FetchedWall } from '@/types/wall-type';
import { Grade, GradeFromValue } from '@/types/Grade/Grade';
import { ScaleName } from '@/types/Grade/scaleName';
import { FetchedUser } from '@/types/user-type';
import { FetchedAscent } from '@/types/ascent-type';
import { getAuthenticatedUser } from '@/auth';
import LogBar from '@/components/log-bar';
import routeRepository from '@/repository/route-repository';
import userRepository from '@/repository/user-repository';
import wallRepository from '@/repository/wall-repository';
import ascentRepository from '@/repository/ascent-repository';
import { Route } from '@/types/route-type';

type RouteDetailPageProps = {
	params: {
		gymId: string;
		climbId: string;
	};
};
const RouteDetailPage = async (props: RouteDetailPageProps) => {
	const route: Route | null = await routeRepository.getRouteById(
		props.params.climbId
	);
	if (route == null) {
		notFound();
	}

	const ascent: FetchedAscent | null =
		await ascentRepository.getAscentByRouteIdAndUserId(
			props.params.climbId,
			(await getAuthenticatedUser()).id
		);
	const author: FetchedUser | null = await userRepository.getAuthorById(
		route?.userId
	);
	const wall: FetchedWall | null = await wallRepository.getWallById(
		route?.wallId
	);
	if (/*author == null ||  ascent == null || */ wall == null) {
		notFound();
	}

	const params: GradeFromValue = { value: route?.grade };
	const grade: Grade = new Grade(params);
	const gradeStr: string = grade.toString(ScaleName.Font);

	return (
		<>
			<TopBar
				header={gradeStr + ' ' + route?.name}
				subheader={'author: ' + author?.name}
				backButtonPath={`/gyms/${props.params.gymId}/climbs`}
			></TopBar>

			<div className="flex w-full items-center justify-center">
				<div className="centered-div">
					<FullWidthScrollableWrapper>
						<HoldsMap
							mode={'readonly'}
							image={wall?.picture ?? ''}
							holds={[]}
							routeHolds={/*route?.holds*/ []}
							wallId={wall?.id}
						/>
					</FullWidthScrollableWrapper>
				</div>
			</div>

			<LogBar
				sent={ascent == null ? false : ascent.sentAt !== null}
				attempts={ascent == null ? 0 : ascent.attempts}
				routeId={route?.id}
				routeDescription={route?.description ?? ''}
				userId={(await getAuthenticatedUser()).id}
			/>
		</>
	);
};

export default RouteDetailPage;
