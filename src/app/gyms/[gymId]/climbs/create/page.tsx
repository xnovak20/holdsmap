'use client';
import React, { useEffect } from 'react';
import { useParams, useRouter } from 'next/navigation';

const CreateClimbPage = () => {
	const router = useRouter();
	const params = useParams<{ gymId: string }>();

	useEffect(() => {
		router.push(`gyms/${params.gymId}/climbs/create/select-wall`);
	}, []);
	return (
		<div>
			<h1>Nothing here bro</h1>
		</div>
	);
};

export default CreateClimbPage;
