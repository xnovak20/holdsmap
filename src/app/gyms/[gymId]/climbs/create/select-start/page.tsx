'use client';
import React, { useContext, useEffect } from 'react';
import { NewRouteContext } from '@/providers/new-route-context-provider';
import { TopBar } from '@/components/top-bar';
import { ArrowRight, Info } from 'lucide-react';
import { useParams, useRouter } from 'next/navigation';
import HoldsMap from '@/components/holds-map/holds-map';
import { FullWidthScrollableWrapper } from '@/components/full-width-scrollable-wrapper';
import { Hold, RouteHold } from '@/types/hold-type';

const SelectStartPage = () => {
	const router = useRouter();
	const newRouteContext = useContext(NewRouteContext);
	const params = useParams<{ gymId: string }>();

	const handleHoldsChange = (updateHolds: Hold[]) => {
		newRouteContext.wallHolds = updateHolds;
	};
	const handleRouteChange = (updatedRoute: RouteHold[]) => {
		newRouteContext.newRouteHolds = updatedRoute;
	};
	useEffect(() => {
		if (newRouteContext.wallId == '') {
			router.push(`/gyms/${params.gymId}/climbs/create/select-wall`);
		} else if (
			newRouteContext.wallHolds.length < 3 ||
			newRouteContext.newRouteHolds.length < 3
		) {
			router.push(`/gyms/${params.gymId}/climbs/create/select-holds`);
		}
	}, []);
	return (
		<div>
			<TopBar
				header={'Select start'}
				subheader={'Select one or two holds'}
				backButtonPath={`/gyms/${params.gymId}/climbs/create/select-holds`}
			>
				<button className={'m-2 p-2'}>
					<Info className={'h-12 w-12 text-black'} />
				</button>
				<button
					className={'m-2 p-2'}
					onClick={() =>
						router.push(
							`/gyms/${params.gymId}/climbs/create/select-top`
						)
					}
				>
					<ArrowRight className={'h-12 w-12 text-black'} />
				</button>
			</TopBar>
			<FullWidthScrollableWrapper>
				<HoldsMap
					image={newRouteContext.wallImage}
					mode={'selectStart'}
					holds={newRouteContext.wallHolds}
					routeHolds={newRouteContext.newRouteHolds}
					onChangeRoute={handleRouteChange}
					onChangeHolds={handleHoldsChange}
					wallId={newRouteContext.wallId}
				/>
			</FullWidthScrollableWrapper>
		</div>
	);
};

export default SelectStartPage;
