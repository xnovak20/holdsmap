'use client';
import React, { useContext, useEffect } from 'react';
import { NewRouteContext } from '@/providers/new-route-context-provider';
import { TopBar } from '@/components/top-bar';
import { ArrowRight, Info } from 'lucide-react';
import { useParams, useRouter } from 'next/navigation';
import HoldsMap from '@/components/holds-map/holds-map';
import { FullWidthScrollableWrapper } from '@/components/full-width-scrollable-wrapper';
import { Hold, RouteHold } from '@/types/hold-type';

const SelectTopPage = () => {
	const router = useRouter();
	const newRouteContext = useContext(NewRouteContext);
	const params = useParams<{ gymId: string }>();

	const handleHoldsChange = (updateHolds: Hold[]) => {
		newRouteContext.wallHolds = updateHolds;
	};
	const handleRouteChange = (updatedRoute: RouteHold[]) => {
		newRouteContext.newRouteHolds = updatedRoute;
	};

	const getStartHoldsCount = (): number => {
		const startHolds = newRouteContext.newRouteHolds.filter(
			(hold) => hold.role === 'start'
		);
		return startHolds.length;
	};

	useEffect(() => {
		if (newRouteContext.wallId == '') {
			router.push(`/gyms/${params.gymId}/climbs/create/select-wall`);
		} else if (
			newRouteContext.wallHolds.length < 3 ||
			newRouteContext.newRouteHolds.length < 3
		) {
			router.push(`/gyms/${params.gymId}/climbs/create/select-holds`);
		}
	}, []);

	return (
		<div>
			<TopBar
				header={'Select top'}
				subheader={'Select one top hold'}
				backButtonPath={`/gyms/${params.gymId}/climbs/create/select-start`}
			>
				<button className={'m-2 p-2'}>
					<Info className={'h-12 w-12 text-black'} />
				</button>
				<button
					className={'m-2 p-2'}
					onClick={() =>
						router.push(
							`/gyms/${params.gymId}/climbs/create/give-details`
						)
					}
				>
					<ArrowRight className={'h-12 w-12 text-black'} />
				</button>
			</TopBar>
			<FullWidthScrollableWrapper>
				<HoldsMap
					image={newRouteContext.wallImage}
					mode={'selectTop'}
					holds={newRouteContext.wallHolds}
					routeHolds={newRouteContext.newRouteHolds}
					onChangeRoute={handleRouteChange}
					onChangeHolds={handleHoldsChange}
					wallId={newRouteContext.wallId}
				/>
			</FullWidthScrollableWrapper>
		</div>
	);
};

export default SelectTopPage;
