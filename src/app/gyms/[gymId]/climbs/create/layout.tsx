import { NewRouteContextProvider } from '@/providers/new-route-context-provider';

const CreateRouteLayout = ({
	children,
}: Readonly<{
	children: React.ReactNode;
}>) => <NewRouteContextProvider>{children}</NewRouteContextProvider>;

export default CreateRouteLayout;
