'use client';
import React, { useContext, useEffect, useState } from 'react';
import { ArrowRight, Info } from 'lucide-react';
import { useParams, useRouter } from 'next/navigation';
import HoldsMap from '@/components/holds-map/holds-map';
import { getWallHoldsAction } from '@/actions/get-wall-holds-action';
import { FullWidthScrollableWrapper } from '@/components/full-width-scrollable-wrapper';
import { NewRouteContext } from '@/providers/new-route-context-provider';
import { TopBar } from '@/components/top-bar';
import { Hold, RouteHold } from '@/types/hold-type';

const SelectHoldsPage = () => {
	const router = useRouter();
	const newRouteContext = useContext(NewRouteContext);
	const params = useParams<{ gymId: string }>();

	//todo hnusny workaround
	const [key, setKey] = useState(0);
	const handleHoldsChange = (updatedHolds: Hold[]) => {
		newRouteContext.wallHolds = updatedHolds;
		setKey(key + 1);
	};
	const handleRouteChange = (updatedRoute: RouteHold[]) => {
		newRouteContext.newRouteHolds = updatedRoute;
		setKey(key + 1);
	};

	const fetchWallHolds = async () => {
		newRouteContext.wallHolds = await getWallHoldsAction(
			newRouteContext.wallId
		);
		setKey(key + 1);

		if (newRouteContext.wallHolds.length < 3) {
			//todo nejaka fallback page, kontaktuj ownera, nejsou vytvorene chyty
		}
	};

	useEffect(() => {
		if (newRouteContext.wallId == '') {
			router.push(`/gyms/${params.gymId}/climbs/create/select-wall`);
		} else if (newRouteContext.wallHolds.length === 0) {
			fetchWallHolds();
		}
	}, []);

	return (
		<div>
			<TopBar
				header={'Select holds'}
				subheader={'Click on holds to add/remove them'}
				backButtonPath={`/gyms/${params.gymId}/climbs/create/select-wall`}
			>
				<button className={'m-2 p-2'}>
					<Info className={'h-12 w-12 text-black'} />
				</button>
				<button
					className={'m-2 p-2'}
					onClick={() =>
						router.push(
							`/gyms/${params.gymId}/climbs/create/select-start`
						)
					}
				>
					<ArrowRight className={'h-12 w-12 text-black'} />
				</button>
			</TopBar>
			<FullWidthScrollableWrapper>
				<HoldsMap
					key={key}
					image={newRouteContext.wallImage}
					mode={'selectHolds'}
					holds={newRouteContext.wallHolds}
					routeHolds={newRouteContext.newRouteHolds}
					onChangeRoute={handleRouteChange}
					onChangeHolds={handleHoldsChange}
					wallId={newRouteContext.wallId}
				/>
			</FullWidthScrollableWrapper>
		</div>
	);
};
export default SelectHoldsPage;
