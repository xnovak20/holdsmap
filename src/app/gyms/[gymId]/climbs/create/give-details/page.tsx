'use client';
import React, { useContext, useEffect } from 'react';
import { TopBar } from '@/components/top-bar';
import { Info } from 'lucide-react';
import { useParams, useRouter } from 'next/navigation';
import { AddRouteForm } from '@/components/forms/create-route-form';
import { NewRouteContext } from '@/providers/new-route-context-provider';

const GiveDetailsPage = () => {
	const router = useRouter();
	const newRouteContext = useContext(NewRouteContext);
	const params = useParams<{ gymId: string }>();

	useEffect(() => {
		if (newRouteContext.wallId == '') {
			router.push(`/gyms/${params.gymId}/climbs/create/select-wall`);
		} else if (
			newRouteContext.wallHolds.length < 3 ||
			newRouteContext.newRouteHolds.length < 3
		) {
			router.push(`/gyms/${params.gymId}/climbs/create/select-holds`);
		}
	}, []);

	return (
		<div>
			<TopBar
				header={'Finish'}
				subheader={'Enter details about new boulder'}
				backButtonPath={`/gyms/${params.gymId}/climbs/create/select-top`}
			>
				<button className={'m-2 p-2'}>
					<Info className={'h-12 w-12 text-black'} />
				</button>
			</TopBar>
			<AddRouteForm
				wallId={newRouteContext.wallId}
				holds={newRouteContext.newRouteHolds}
			/>
		</div>
	);
};

export default GiveDetailsPage;
