'use client';

import { type PropsWithChildren } from 'react';
import { NewRouteContextProvider } from '@/providers/new-route-context-provider';

export const Providers = ({ children }: PropsWithChildren) => (
	<NewRouteContextProvider>{children}</NewRouteContextProvider>
);
