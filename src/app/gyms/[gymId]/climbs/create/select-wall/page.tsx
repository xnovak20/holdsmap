'use client';
import React, { useContext, useEffect, useState } from 'react';
import { NewRouteContext } from '@/providers/new-route-context-provider';
import { TopBar } from '@/components/top-bar';
import { Info } from 'lucide-react';
import { useParams, useRouter } from 'next/navigation';
import { FetchedWall } from '@/types/wall-type';
import { getAllWallsByGymIdAction } from '@/actions/get-all-walls-action';

const SelectWallPage = () => {
	const router = useRouter();
	const params = useParams<{ gymId: string }>();
	const newRouteContext = useContext(NewRouteContext);
	const [walls, setWalls] = useState<FetchedWall[]>([]);

	const handleWallSelect = (wall: FetchedWall) => {
		newRouteContext.wallId = wall.id;
		newRouteContext.wallImage = wall.picture;
		newRouteContext.newRouteHolds = [];
		newRouteContext.wallHolds = [];
		router.push(`/gyms/${params.gymId}/climbs/create/select-holds`);
	};

	useEffect(() => {
		getAllWallsByGymIdAction(params.gymId).then((res: FetchedWall[]) =>
			setWalls(res)
		);
	}, []);
	/* todo predelat zobrazeni gymu na karticku podobnou v seznamu walls */
	return (
		<div>
			<TopBar
				header={'Select wall'}
				subheader={'On which wall will this climb be?'}
				backButtonPath={`/gyms/${params.gymId}/climbs`}
			>
				<button className={'m-2 p-2'}>
					<Info className={'h-12 w-12 text-black'} />
				</button>
			</TopBar>
			{walls.length === 0 ? (
				<p>No walls in this gym</p>
			) : (
				walls.map((wall) => {
					return (
						<button
							key={wall.id}
							className={
								'm-4 rounded-xl bg-secondary p-2 px-4 text-3xl'
							}
							onClick={() => handleWallSelect(wall)}
						>
							{wall.name}
						</button>
					);
				})
			)}
		</div>
	);
};

export default SelectWallPage;
