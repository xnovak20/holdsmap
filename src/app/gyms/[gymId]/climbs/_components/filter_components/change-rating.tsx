import { Button } from '@/components/ui/button';
import RemoveImage from '@/components/images/remove';
import Add from '@/components/images/add-image';
import Star from '@/components/images/star';
import {
	changeRatingEnum,
	Direction,
	MAX_RATING,
	MIN_RATING,
	FormContext,
} from '../filter-utils';
import { useContext } from 'react';
import { FilterContext } from '@/app/gyms/[gymId]/climbs/_components/filter';

type ChangeRatingProps = {
	title: string;
	currentValue: number;
};

const ChangeRating = ({ title, currentValue }: ChangeRatingProps) => {
	const { filter, setFilter } = useContext(FilterContext) as FormContext;

	const changeValue = (type: changeRatingEnum) => {
		const isDecrease =
			type === changeRatingEnum.DecreaseFrom ||
			type === changeRatingEnum.DecreaseTo;
		const isFrom =
			type === changeRatingEnum.IncreaseFrom ||
			type === changeRatingEnum.DecreaseFrom;
		const rating = isFrom ? filter.ratingFrom : filter.ratingTo;

		if (
			(isDecrease && rating === MIN_RATING) ||
			(!isDecrease && rating === MAX_RATING)
		)
			return;

		setFilter((prevFilters) => ({
			...prevFilters,
			[isFrom ? 'ratingFrom' : 'ratingTo']:
				rating + (isDecrease ? Direction.DOWN : Direction.UP),
		}));
	};

	const increase =
		title === 'Rating From'
			? changeRatingEnum.IncreaseFrom
			: changeRatingEnum.IncreaseTo;
	const decrease =
		title === 'Rating From'
			? changeRatingEnum.DecreaseFrom
			: changeRatingEnum.DecreaseTo;

	return (
		<span className="">
			<span className="flex items-center justify-center">{title}</span>
			<span className="flex items-center justify-center">
				<Button
					className="bg-transparent text-primary"
					onClick={() => changeValue(decrease)}
				>
					<RemoveImage />
				</Button>
				<span className="text-primary"> {currentValue}</span>
				<Star />

				<Button
					className="bg-transparent text-primary"
					onClick={() => changeValue(increase)}
				>
					<Add />
				</Button>
			</span>
		</span>
	);
};

export default ChangeRating;
