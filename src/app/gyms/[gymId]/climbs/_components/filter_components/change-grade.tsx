import { useContext } from 'react';
import { Button } from '@/components/ui/button';
import Remove from '@/components/images/remove';
import Add from '@/components/images/add-image';
import { changeGradeEnum, Direction } from '../filter-utils';
import { MIN_GRADE, MAX_GRADE, FormContext } from '../filter-utils';
import { FilterContext } from '@/app/gyms/[gymId]/climbs/_components/filter';

type DifficultyProps = {
	title: string;
	currentValue: string;
};

const ChangeGrade = ({ title, currentValue }: DifficultyProps) => {
	const { setFilter, gradeFrom, gradeTo } = useContext(
		FilterContext
	) as FormContext;

	const changeValue = (type: changeGradeEnum) => {
		const targetGradeKey =
			type === changeGradeEnum.IncreaseTo ||
			type === changeGradeEnum.DecreaseTo
				? 'gradeTo'
				: 'gradeFrom';
		const targetGrade =
			type === changeGradeEnum.IncreaseTo ||
			type === changeGradeEnum.DecreaseTo
				? gradeTo
				: gradeFrom;
		const direction =
			type === changeGradeEnum.IncreaseFrom ||
			type === changeGradeEnum.IncreaseTo
				? Direction.UP
				: Direction.DOWN;

		if (targetGrade.getValue() === MAX_GRADE && direction === Direction.UP)
			return;
		if (
			targetGrade.getValue() === MIN_GRADE &&
			direction === Direction.DOWN
		)
			return;

		setFilter((prevFilters) => ({
			...prevFilters,
			[targetGradeKey]: targetGrade.getValue() + direction,
		}));
	};

	const increase =
		title === 'Difficulty From'
			? changeGradeEnum.IncreaseFrom
			: changeGradeEnum.IncreaseTo;
	const decrease =
		title === 'Difficulty From'
			? changeGradeEnum.DecreaseFrom
			: changeGradeEnum.DecreaseTo;

	return (
		<span className="rounded-lg pb-2 pt-2">
			<span className="flex items-center justify-center">{title}</span>
			<span className="flex items-center justify-center">
				<Button
					className="bg-transparent text-primary"
					onClick={() => changeValue(decrease)}
				>
					<Remove />
				</Button>
				<span className="text-primary">{currentValue}</span>
				<Button
					className="bg-transparent text-primary"
					onClick={() => changeValue(increase)}
				>
					<Add />
				</Button>
			</span>
		</span>
	);
};

export default ChangeGrade;
