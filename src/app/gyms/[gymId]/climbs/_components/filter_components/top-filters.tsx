import { Button } from '@/components/ui/button';
import { useContext } from 'react';
import { FilterContext } from '@/app/gyms/[gymId]/climbs/_components/filter';
import { FormContext } from '../filter-utils';
import MenuImage from '@/components/images/menu';
import { ClimbFilter } from '@/app/gyms/[gymId]/climbs/_components/filter-utils';

type TopFiltersProps = {
	openDialog: () => void;
	saveAndClose: () => void;
};

const TopFilters = ({ openDialog, saveAndClose }: TopFiltersProps) => {
	const { filter, setFilter } = useContext(FilterContext) as FormContext;

	const handleOnClickNew = () => {
		setFilter((prevFilter: ClimbFilter) => ({
			...prevFilter,
			new: !prevFilter.new,
		}));
		saveAndClose();
	};

	const handleOnClickNewBookMarks = () => {
		setFilter((prevFilter: ClimbFilter) => ({
			...prevFilter,
			bookmarks: !filter.bookmarks,
		}));
		saveAndClose();
	};

	return (
		<div className="flex flex-grow gap-2 p-2 pl-4 pr-4">
			<Button
				className={`flex-grow ${
					filter.new
						? 'bg-primary text-white'
						: 'border border-primary text-primary'
				}`}
				onClick={() => handleOnClickNew()}
			>
				{' '}
				New
			</Button>
			<Button
				className={`flex-grow ${
					filter.bookmarks
						? 'bg-primary text-white'
						: 'border border-primary text-primary'
				}`}
				onClick={() => handleOnClickNewBookMarks()}
			>
				{' '}
				Bookmarks
			</Button>
			<Button
				className="flex-grow"
				variant={'primary'}
				onClick={() => openDialog()}
			>
				<MenuImage />
			</Button>
		</div>
	);
};

export default TopFilters;
