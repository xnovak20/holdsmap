import { useContext } from 'react';
import { Button } from '@/components/ui/button';
import {
	buttonGroupOne,
	buttonGroupTwo1,
	buttonGroupTwo2,
	FormContext,
} from '@/app/gyms/[gymId]/climbs/_components/filter-utils';
import { FilterContext } from '@/app/gyms/[gymId]/climbs/_components/filter';
import { ClimbFilter } from '@/app/gyms/[gymId]/climbs/_components/filter-utils';

const DialogButtons = () => {
	const { filter, setFilter } = useContext(FilterContext) as FormContext;

	const handleClick = (name: string, property: keyof ClimbFilter) => {
		setFilter((prevFilters: ClimbFilter) => ({
			...prevFilters,
			[property]: name === prevFilters[property] ? '' : name,
		}));
	};

	return (
		<>
			<div className="mt-1 flex justify-center rounded-2xl border">
				<div className="mb-2 mt-2">
					{buttonGroupOne.map((button) => (
						<Button
							key={button}
							className={`${filter.activeButtonGroupOne === button ? 'bg-primary text-white' : 'border border-primary text-primary'} m-1`}
							onClick={() =>
								handleClick(button, 'activeButtonGroupOne')
							}
						>
							{button}
						</Button>
					))}
				</div>
			</div>
			<div className="mt-1 justify-center rounded-2xl border">
				<div className="mb-2 mt-2">
					{buttonGroupTwo1.map((button, _index) => (
						<Button
							key={button}
							className={`${filter.activeButtonGroupTwo === button ? 'bg-primary text-white' : 'border border-primary text-primary'} m-1`}
							onClick={() =>
								handleClick(button, 'activeButtonGroupTwo')
							}
						>
							{button}
						</Button>
					))}
					{buttonGroupTwo2.map((button, _index) => (
						<Button
							key={button}
							className={`${filter.activeButtonGroupTwo === button ? 'bg-primary text-white' : 'border border-primary text-primary'} m-1`}
							onClick={() =>
								handleClick(button, 'activeButtonGroupTwo')
							}
						>
							{button}
						</Button>
					))}
				</div>
			</div>
		</>
	);
};

export default DialogButtons;
