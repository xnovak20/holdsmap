import { useContext } from 'react';
import { Button } from '@/components/ui/button';
import {
	initialFilter,
	FormContext,
} from '@/app/gyms/[gymId]/climbs/_components/filter-utils';
import { FilterContext } from '@/app/gyms/[gymId]/climbs/_components/filter';

type ActionButtonsProps = {
	saveAndClose: () => void;
};

const ActionButtons = ({ saveAndClose }: ActionButtonsProps) => {
	const { setFilter } = useContext(FilterContext) as FormContext;
	const resetFilter = () => setFilter(initialFilter);

	return (
		<div className="flex justify-center">
			<div className="flex justify-center gap-4">
				<Button
					onClick={resetFilter}
					className="w-32 border border-primary text-primary"
				>
					Clear
				</Button>
				<Button
					onClick={saveAndClose}
					type="submit"
					variant="primary"
					className="w-32"
				>
					Submit
				</Button>
			</div>
		</div>
	);
};

export default ActionButtons;
