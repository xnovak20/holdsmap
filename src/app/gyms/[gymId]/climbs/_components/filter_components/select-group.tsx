import {
	Select,
	SelectContent,
	SelectItem,
	SelectTrigger,
	SelectValue,
} from '@/components/ui/select';
import ListBulletImage from '@/components/images/list-bullet';
import { wall } from '@/db/schema/wall';
import { user } from '@/db/schema/user';
import { useContext } from 'react';
import { FilterContext } from '@/app/gyms/[gymId]/climbs/_components/filter';
import {
	ClimbFilter,
	FormContext,
} from '@/app/gyms/[gymId]/climbs/_components/filter-utils';

type User = typeof user.$inferSelect;
type Wall = typeof wall.$inferSelect;

type SelectGroupProps = {
	users: User[];
	walls: Wall[];
};

const SelectGroup = ({ users, walls }: SelectGroupProps) => {
	const { filter, setFilter } = useContext(FilterContext) as FormContext;

	const handleChange = (id: string, property: string) => {
		setFilter((prevFilters: ClimbFilter) => ({
			...prevFilters,
			[property]: id,
		}));
	};

	return (
		<>
			<span className="flex justify-start pb-1 pt-1">Wall</span>
			<div className="flex justify-center pb-1 pt-1">
				<Select
					defaultValue={filter.wallId}
					onValueChange={(value) => handleChange(value, 'wallId')}
				>
					<SelectTrigger>
						<SelectValue placeholder={<ListBulletImage />} />
					</SelectTrigger>
					<SelectContent>
						{walls.map((wall) => (
							<SelectItem key={wall.id} value={wall.id}>
								{wall.name}
							</SelectItem>
						))}
					</SelectContent>
				</Select>
			</div>
			<span className="flex justify-start pb-1 pt-1">Author</span>
			<div className="flex justify-center pb-1">
				<Select
					defaultValue={filter.authorId}
					onValueChange={(value) => handleChange(value, 'authorId')}
				>
					<SelectTrigger>
						<SelectValue placeholder={<ListBulletImage />} />
					</SelectTrigger>
					<SelectContent>
						{users.map((user) => (
							<SelectItem key={user.id} value={user.id}>
								{user.name}
							</SelectItem>
						))}
					</SelectContent>
				</Select>
			</div>
			<span className="flex justify-start pb-1 pt-1"> Climbed By </span>
			<div className="flex justify-center pb-2">
				<Select
					defaultValue={filter.climbedById}
					onValueChange={(value) =>
						handleChange(value, 'climbedById')
					}
				>
					<SelectTrigger>
						<SelectValue placeholder={<ListBulletImage />} />
					</SelectTrigger>
					<SelectContent>
						{users.map((user) => (
							<SelectItem key={user.id} value={user.id}>
								{user.name}
							</SelectItem>
						))}
					</SelectContent>
				</Select>
			</div>
		</>
	);
};

export default SelectGroup;
