import React, { useContext } from 'react';
import { Button } from '@/components/ui/button';
import { ScaleName } from '@/types/Grade/scaleName';
import { FilterContext } from '@/app/gyms/[gymId]/climbs/_components/filter';
import {
	MAX_GRADE,
	MAX_RATING,
	MIN_GRADE,
	MIN_RATING,
	FormContext,
} from '@/app/gyms/[gymId]/climbs/_components/filter-utils';
import Delete from '@/components/images/delete';

type FilterLabelWithDeleteProps = {
	type: string;
	label: string | number;
	onDelete: (type: string) => void;
};

const FilterLabelWithDelete = ({
	type,
	label,
	onDelete,
}: FilterLabelWithDeleteProps) => {
	let finalLabel = label;

	switch (type) {
		case 'ratingFrom':
			finalLabel = '>= ' + label.toString() + '☆ ';
			break;
		case 'ratingTo':
			finalLabel = '<= ' + label.toString() + '☆ ';
			break;
		case 'wallId':
			finalLabel = 'Wall: ' + label + ' ';
			break;
		case 'authorId':
			finalLabel = 'Author: ' + label + ' ';
			break;
		case 'climbedById':
			finalLabel = 'Climber: ' + label + ' ';
			break;
		case 'gradeFrom':
			finalLabel = '>= ' + label + ' ';
			break;
		case 'gradeTo':
			finalLabel = '<= ' + label + ' ';
			break;
	}

	return (
		<Button
			onClick={() => onDelete(type)}
			variant="primary"
			className="flex items-center p-2"
		>
			<span className="mr-1">{finalLabel}</span>
			<Delete />
		</Button>
	);
};

type FilterLabelsProps = {
	getNameById: (id: string | undefined, type: string) => string | undefined;
	handleFilterLabelDelete: (type: string) => void;
};

const FilterLabels = ({
	getNameById,
	handleFilterLabelDelete,
}: FilterLabelsProps) => {
	const { filter, gradeFrom, gradeTo } = useContext(
		FilterContext
	) as FormContext;

	const labels = [
		{
			type: 'gradeFrom',
			value: gradeFrom.toString(ScaleName.Font),
			condition: filter.gradeFrom !== MIN_GRADE,
		},
		{
			type: 'gradeTo',
			value: gradeTo.toString(ScaleName.Font),
			condition: filter.gradeTo !== MAX_GRADE,
		},
		{
			type: 'ratingFrom',
			value: filter.ratingFrom,
			condition: filter.ratingFrom !== MIN_RATING,
		},
		{
			type: 'ratingTo',
			value: filter.ratingTo,
			condition: filter.ratingTo !== MAX_RATING,
		},
		{
			type: 'activeButtonGroupOne',
			value: filter.activeButtonGroupOne,
			condition: filter.activeButtonGroupOne !== '',
		},
		{
			type: 'activeButtonGroupTwo',
			value: filter.activeButtonGroupTwo,
			condition: filter.activeButtonGroupTwo !== '',
		},
		{
			type: 'wallId',
			value: getNameById(filter.wallId, 'wall'),
			condition: filter.wallId !== undefined,
		},
		{
			type: 'authorId',
			value: getNameById(filter.authorId, 'author'),
			condition: filter.authorId !== undefined,
		},
		{
			type: 'climbedById',
			value: getNameById(filter.climbedById, 'climbedById'),
			condition: filter.climbedById !== undefined,
		},
	];

	return (
		<div className="flex flex-wrap gap-2">
			<div className="flex flex-wrap gap-2 pl-4 pr-4">
				{labels.map(
					({ type, value, condition }) =>
						condition && (
							<FilterLabelWithDelete
								key={type}
								type={type}
								label={value || ''}
								onDelete={handleFilterLabelDelete}
							/>
						)
				)}
			</div>
		</div>
	);
};

export default FilterLabels;
