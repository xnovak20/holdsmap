'use client';

import { route } from '@/db/schema/route';
import { ascent } from '@/db/schema/ascent';
import { Grade } from '@/types/Grade/Grade';
import { ScaleName } from '@/types/Grade/scaleName';
import _ from 'lodash';
import Tick from '@/components/images/tick';
import StarMini from '@/components/images/star-mini';
import { User } from '@/db/schema/user';

type Route = typeof route.$inferSelect;
type Ascent = typeof ascent.$inferSelect;

type ClimbCardProps = {
	route: Route;
	ascents: Ascent[];
	routeAuthor: (id: string | undefined, type: string) => string | undefined;
	currentUser: User;
};

const ClimbCard = ({
	route,
	ascents,
	routeAuthor,
	currentUser,
}: ClimbCardProps) => {
	const grade = new Grade({ value: route.grade });
	const filteredAscents = ascents.filter(
		(ascent) => ascent.routeId === route.id
	);
	const triedToClimb = filteredAscents.filter(
		(ascent) => ascent.userId === currentUser.id
	);

	const calculateAvgRating = () => {
		if (filteredAscents.length > 0) {
			return Math.round(
				_.meanBy(filteredAscents, (ascent) => ascent.stars)
			);
		} else {
			return 0;
		}
	};

	const renderRatingStars = () => {
		return Array.from({ length: calculateAvgRating() }, (_, i) => (
			<StarMini starIndex={i} key={i} />
		));
	};

	return (
		<div className="relative gap-2 rounded-xl border-gray-100 px-4 py-5 shadow transition-all hover:scale-105 hover:shadow-lg">
			<div className="flex pb-2">
				<span className="text-5xl font-bold text-primary">
					{grade.toString(ScaleName.Font)}
				</span>
				<div className="pl-2">
					<div className="font-bold">{route.name}</div>
					<div>Author: {routeAuthor(route.userId!, 'user')}</div>
				</div>
			</div>
			<div className="flex gap-4">
				{triedToClimb.length > 0 && (
					<span className="flex gap-1">
						<Tick />
					</span>
				)}
				<span className="flex gap-1">{renderRatingStars()}</span>
			</div>
			You have tried to climb this {triedToClimb.length} times.
		</div>
	);
};

export default ClimbCard;
