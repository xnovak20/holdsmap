import { route } from '@/db/schema/route';
import { ascent } from '@/db/schema/ascent';
import { user } from '@/db/schema/user';
import { wall } from '@/db/schema/wall';
import { Dispatch, SetStateAction } from 'react';
import { Grade } from '@/types/Grade/Grade';

export const buttonGroupOne = ['Not Tried', 'Project', 'Climbed'];
export const buttonGroupTwo1 = ['Random', 'Oldest', 'Worst'];
export const buttonGroupTwo2 = ['Best', 'Easiest', 'Hardest'];

export enum changeGradeEnum {
	IncreaseFrom,
	DecreaseFrom,
	IncreaseTo,
	DecreaseTo,
}

export type Climbs = {
	route: Route;
	ascent: Ascent;
};

export enum changeRatingEnum {
	IncreaseFrom,
	DecreaseFrom,
	IncreaseTo,
	DecreaseTo,
}

export const MAX_GRADE = 12;
export const MIN_GRADE = 1;

export const MAX_RATING = 5;
export const MIN_RATING = 1;

export enum Direction {
	UP = 1,
	DOWN = -1,
}

export type RouteAscents = {
	routeRating: number;
	route: Route;
	ascents: Ascent[];
};

export type FilterProps = {
	routeAscents: RouteAscents[];
	users: User[];
	walls: Wall[];
	currentUser: User;
};

export type User = typeof user.$inferSelect;
export type Wall = typeof wall.$inferSelect;
export type Route = typeof route.$inferSelect;
export type Ascent = typeof ascent.$inferSelect;

export type FormContext = {
	filter: ClimbFilter;
	setFilter: Dispatch<SetStateAction<ClimbFilter>>;
	gradeFrom: Grade;
	gradeTo: Grade;
};

export type ClimbFilter = {
	gradeFrom: number;
	gradeTo: number;
	ratingFrom: number;
	ratingTo: number;
	activeButtonGroupOne: string;
	activeButtonGroupTwo: string;
	authorId: string | undefined;
	climbedById: string | undefined;
	wallId: string | undefined;
	new: boolean;
	bookmarks: boolean;
};

export const initialFilter: ClimbFilter = {
	gradeFrom: MIN_GRADE,
	gradeTo: MAX_GRADE,
	ratingFrom: MIN_RATING,
	ratingTo: MAX_RATING,
	activeButtonGroupOne: '',
	activeButtonGroupTwo: '',
	authorId: undefined,
	climbedById: undefined,
	wallId: undefined,
	new: false,
	bookmarks: false,
};
