import {
	ClimbFilter,
	RouteAscents,
} from '@/app/gyms/[gymId]/climbs/_components/filter-utils';
import _ from 'lodash';
import { User } from '@/db/schema/user';

const calculateAvgRating = (routeAscents_: RouteAscents) => {
	if (routeAscents_.ascents.length > 0) {
		return Math.round(
			_.meanBy(routeAscents_.ascents, (ascent) => ascent.stars)
		);
	} else {
		return 0;
	}
};

const ApplyFilteringAndOrdering = (
	filter: ClimbFilter,
	routeAscents: RouteAscents[],
	currentUser: User
) => {
	let filteredData: RouteAscents[] = [];

	// filtering logic
	routeAscents.forEach((routeAscents_, _i) => {
		const triedToClimb = routeAscents_.ascents.filter(
			(ascent) => ascent.userId === currentUser.id
		);
		routeAscents_.routeRating = calculateAvgRating(routeAscents_);

		if (
			(routeAscents_.route.grade !== null &&
				!(
					routeAscents_.route.grade >= filter.gradeFrom &&
					routeAscents_.route.grade <= filter.gradeTo
				)) ||
			(filter.authorId !== undefined &&
				!(routeAscents_.route.userId === filter.authorId)) ||
			(filter.wallId !== undefined &&
				!(routeAscents_.route.wallId === filter.wallId)) ||
			(filter.activeButtonGroupOne === 'Not Tried' &&
				!(triedToClimb.length !== 0)) ||
			(filter.activeButtonGroupOne === 'Climbed' &&
				!(triedToClimb.length === 0)) ||
			(routeAscents_.routeRating > 0 &&
				!(
					routeAscents_.routeRating >= filter.ratingFrom &&
					routeAscents_.routeRating <= filter.ratingTo
				))
		) {
			return;
		}

		filteredData.push(routeAscents_);
	});

	let sortedFilteredData: RouteAscents[] = filteredData;

	// ordering logic
	switch (filter.activeButtonGroupTwo) {
		case 'Oldest':
			sortedFilteredData = _.orderBy(
				filteredData,
				(item) => item.route.createdAt,
				'desc'
			);
			break;
		case 'Best':
			sortedFilteredData = _.orderBy(
				filteredData,
				(item) => item.routeRating,
				'desc'
			);
			break;
		case 'Worst':
			sortedFilteredData = _.orderBy(
				filteredData,
				(item) => item.routeRating,
				'asc'
			);
			break;
		case 'Easiest':
			sortedFilteredData = _.orderBy(
				filteredData,
				(item) => item.route.grade,
				'asc'
			);
			break;
		case 'Hardest':
			sortedFilteredData = _.orderBy(
				filteredData,
				(item) => item.route.grade,
				'desc'
			);
			break;
	}

	if (filter.new) {
		sortedFilteredData = _.orderBy(
			filteredData,
			(item) => item.route.createdAt,
			['desc']
		);
	}

	return sortedFilteredData;
};

export default ApplyFilteringAndOrdering;
