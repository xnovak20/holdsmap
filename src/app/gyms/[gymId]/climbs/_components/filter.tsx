'use client';

import { createContext, useEffect, useState } from 'react';
import { useSearchParams } from 'next/navigation';
import { Grade } from '@/types/Grade/Grade';
import { ScaleName } from '@/types/Grade/scaleName';
import {
	ClimbFilter,
	FilterProps,
	FormContext,
	initialFilter,
	MAX_RATING,
	MIN_RATING,
	RouteAscents,
} from './filter-utils';

import ChangeGrade from '@/app/gyms/[gymId]/climbs/_components/filter_components/change-grade';
import Rating from '@/app/gyms/[gymId]/climbs/_components/filter_components/change-rating';
import FilterLabelWithDelete from '@/app/gyms/[gymId]/climbs/_components/filter_components/filter-labels';
import TopFilters from '@/app/gyms/[gymId]/climbs/_components/filter_components/top-filters';
import DialogButtons from '@/app/gyms/[gymId]/climbs/_components/filter_components/dialog-buttons';
import SelectGroup from '@/app/gyms/[gymId]/climbs/_components/filter_components/select-group';
import ActionButtons from '@/app/gyms/[gymId]/climbs/_components/filter_components/action-buttons';

import {
	Dialog,
	DialogContent,
	DialogDescription,
	DialogFooter,
	DialogHeader,
	DialogTitle,
} from '@/components/ui/dialog';
import { ScrollArea } from '@/components/ui/scroll-area';
import ClimbCard from '@/app/gyms/[gymId]/climbs/_components/climb-card';
import ApplyFilteringAndOrdering from '@/app/gyms/[gymId]/climbs/_components/filtering-ordering-logic';
import { ClipLoader } from 'react-spinners';

export const FilterContext = createContext<FormContext | null>(null);

const Filter = ({ users, walls, currentUser, routeAscents }: FilterProps) => {
	const searchParams = useSearchParams();
	const queryParamWallId = searchParams.get('wallId');

	const [isDialogOpen, setIsDialogOpen] = useState<boolean>(false);
	const [filter, setFilter] = useState<ClimbFilter>(initialFilter);
	const gradeFrom = new Grade({ value: filter.gradeFrom });
	const gradeTo = new Grade({ value: filter.gradeTo });
	const [loading, setLoading] = useState(true);
	const [filteredData, setFilteredData] =
		useState<RouteAscents[]>(routeAscents);

	const openDialog = () => setIsDialogOpen(true);
	const saveAndClose = () => {
		setIsDialogOpen(false);
		setLoading(true);
		setFilteredData(
			ApplyFilteringAndOrdering(filter, routeAscents, currentUser)
		);
		setLoading(false);
	};

	useEffect(() => {
		setFilter((prevState) => ({
			...prevState,
			wallId: queryParamWallId ?? undefined,
		}));
		setFilteredData(
			ApplyFilteringAndOrdering(
				{ ...filter, wallId: queryParamWallId ?? undefined },
				routeAscents,
				currentUser
			)
		);
		setLoading(false);
	}, []);

	const handleFilterLabelDelete = (type: string) => {
		let updatedFilter;
		switch (type) {
			case 'ratingFrom':
			case 'ratingTo':
				updatedFilter = {
					...filter,
					[type]: type === 'ratingFrom' ? MIN_RATING : MAX_RATING,
				};
				break;
			case 'gradeFrom':
			case 'gradeTo':
				updatedFilter = {
					...filter,
					[type]: type === 'gradeFrom' ? 1 : 12,
				};
				break;
			case 'activeButtonGroupOne':
			case 'activeButtonGroupTwo':
				updatedFilter = { ...filter, [type]: '' };
				break;
			default:
				updatedFilter = { ...filter, [type]: undefined };
				break;
		}
		setFilter(updatedFilter);
		setFilteredData(
			ApplyFilteringAndOrdering(updatedFilter, routeAscents, currentUser)
		);
	};

	const getNameById = (id: string | undefined, type: string) => {
		return type === 'wall'
			? walls.find((wall) => wall.id === id)?.name
			: users.find((user) => user.id === id)?.name;
	};

	return (
		<FilterContext.Provider
			value={{ filter, setFilter, gradeFrom, gradeTo }}
		>
			<Dialog open={isDialogOpen} onOpenChange={setIsDialogOpen}>
				<TopFilters
					openDialog={openDialog}
					saveAndClose={saveAndClose}
				/>
				<FilterLabelWithDelete
					getNameById={getNameById}
					handleFilterLabelDelete={handleFilterLabelDelete}
				/>
				<DialogContent>
					<ScrollArea className="h-[650px] flex-1">
						<DialogHeader>
							<DialogTitle className="flex justify-center pb-2">
								Filter
							</DialogTitle>
							<DialogDescription>
								<div className="m-4">
									<div className="flex justify-center gap-1 rounded-2xl border">
										<ChangeGrade
											title="Difficulty From"
											currentValue={gradeFrom.toString(
												ScaleName.Font
											)}
										/>
										<ChangeGrade
											title="Difficulty To"
											currentValue={gradeTo.toString(
												ScaleName.Font
											)}
										/>
									</div>
									<DialogButtons />
									<div className="mt-1 flex justify-center gap-2 rounded-2xl border p-2">
										<Rating
											title="Rating From"
											currentValue={filter.ratingFrom}
										/>
										<Rating
											title="Rating To"
											currentValue={filter.ratingTo}
										/>
									</div>
									<SelectGroup walls={walls} users={users} />
								</div>
							</DialogDescription>
						</DialogHeader>
						<DialogFooter>
							<ActionButtons saveAndClose={saveAndClose} />
						</DialogFooter>
					</ScrollArea>
				</DialogContent>
			</Dialog>
			{loading && (
				<div className="flex items-center justify-center pt-24">
					<ClipLoader size={50} color={'#0ea5e9'} />
				</div>
			)}
			{!loading && (
				<div className="pl-4 pr-4">
					{filteredData.map(
						(routeAscents: RouteAscents, index: number) => (
							<ClimbCard
								routeAuthor={getNameById}
								currentUser={currentUser}
								route={routeAscents.route}
								ascents={routeAscents.ascents}
								key={index}
							/>
						)
					)}
				</div>
			)}
		</FilterContext.Provider>
	);
};

export default Filter;
