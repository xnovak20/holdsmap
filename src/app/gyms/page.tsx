import React from 'react';
import { GymPageView } from '@/components/page-views/gym-page-view';
import { GymList } from '@/components/gym/gym-list';
import { GymSearchBar } from '@/components/gym/gym-search-bar';
import { getAuthenticatedUser } from '@/auth';
import Link from 'next/link';
import { PlusIcon } from 'lucide-react';
import { Breadcrumbs } from '@/components/breadcrumbs';
import { Logout } from '@/components/logout';
import { FetchedUser } from '@/types/user-type';

const GymsPage = async ({ searchParams }: { searchParams: { q?: string } }) => {
	const user: FetchedUser = await getAuthenticatedUser();

	return (
		<GymPageView
			title="Gyms"
			subtitle="Select gym"
			actions={
				<>
					<Link
						href={'/gyms/create'}
						className="block size-auto items-center justify-center p-2 text-white hover:animate-spin"
					>
						<PlusIcon className="size-10" />
					</Link>
					<Logout />
				</>
			}
		>
			{user.email}
			<Breadcrumbs items={[{ title: 'Gyms' }]} />

			<div className="space-y-8">
				<GymSearchBar />
				<GymList query={searchParams.q} user={user} />
			</div>
		</GymPageView>
	);
};

export default GymsPage;
