'use client';

import React from 'react';

export default function Error({
	error,
	reset,
}: {
	error: Error & { digest?: string };
	reset: () => void;
}) {
	const errorTitles = [
		'Internal Server Meltdown',
		'The Server Gremlins Are at It Again',
		'Error 500: Server Out of Whack',
		'Oops! The Server Seems to Be on Strike',
		'500: Internal Server Overload',
		"Server Error: This Wasn't in the Script!",
		'The Cybernetic Brain Has Overheated',
		'500: Server Insanity in Progress',
		'Server on the Blink: Please Stand By',
		'500: Server Short Circuit Detected',
	];
	const randomIndex = Math.floor(Math.random() * errorTitles.length);

	return (
		<div
			className="flex min-h-screen flex-col items-center justify-center bg-gradient-to-t from-cyan-500 to-blue-500
			 pl-4 pr-4 text-white"
		>
			<h1 className="bg-gradient-to-r from-purple-400 to-pink-600 bg-clip-text text-8xl font-extrabold text-transparent contrast-125">
				500!
			</h1>
			<h2 className="text-md bg-clip-text font-extrabold text-white contrast-125">
				{errorTitles[randomIndex]}
			</h2>
			<p>{error.message}</p>
		</div>
	);
}
