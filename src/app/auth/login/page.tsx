import LoginForm from '@/components/auth/login-form';
import { Title } from '@/components/auth/title';

const LoginPage = async () => {
	return (
		<>
			<Title>Login</Title>

			<LoginForm />
		</>
	);
};

export default LoginPage;
