import { FC, PropsWithChildren } from 'react';
import { auth } from '@/auth';
import { redirect } from 'next/navigation';
import app from '@/config/app';

//todo nemá toto být v layout.tsx?
const AuthLayout: FC<PropsWithChildren> = async ({ children }) => {
	const session = await auth();

	if (session && session.user) {
		redirect(app.homePath);
	}

	return (
		<>
			<div className="mx-auto flex min-h-screen max-w-sm items-center justify-center py-8">
				<div className="w-full space-y-4 px-4">{children}</div>
			</div>
		</>
	);
};

export default AuthLayout;
