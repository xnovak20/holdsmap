import RegisterForm from '@/components/auth/register-form';
import { Title } from '@/components/auth/title';

const RegisterPage = () => {
	return (
		<>
			<Title>Register</Title>

			<RegisterForm />
		</>
	);
};

export default RegisterPage;
