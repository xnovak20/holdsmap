export type EditorMode =
	| 'createHolds'
	| 'createVolumes'
	| 'selectHolds'
	| 'selectVolumes'
	| 'selectStart'
	| 'selectTop'
	| 'readonly';
