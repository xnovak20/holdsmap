import { FormEventHandler } from 'react';

export type FormFieldProps = {
	label: string;
	name: string;
	id?: string;
	placeholder?: string;
	autoComplete?: string;

	value?: HTMLInputElement['value'];
	defaultValue?: HTMLInputElement['defaultValue'];
	onChange?: FormEventHandler;
	onBlur?: FormEventHandler;
	disabled?: boolean;
};

export type MultiClassName<TIdentifier extends string> = {
	className?:
		| {
				[k in TIdentifier]?: string;
		  }
		| string;
};
export type PageProps<
	TParams extends Record<string, string> = {},
	TSearchParams extends Record<string, string | string[] | undefined> = {},
> = {
	params: TParams;
	searchParams: TSearchParams;
};
