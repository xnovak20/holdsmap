import { gym } from '@/db/schema/gym';

export type Gym = typeof gym.$inferSelect;
