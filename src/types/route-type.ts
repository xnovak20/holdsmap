import { route } from '@/db/schema/route';
import { RouteHold } from '@/types/hold-type';

export type FetchedRoute = typeof route.$inferSelect;
export type InsertRoute = typeof route.$inferInsert;

export type Route = FetchedRoute & {
	holds: RouteHold[];
};
