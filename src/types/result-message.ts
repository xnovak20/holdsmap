export type ResultMessage = { error: string } | { success: string };
