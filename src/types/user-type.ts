import { user } from '@/db/schema/user';

export type FetchedUser = typeof user.$inferSelect;
export type InsertUser = typeof user.$inferInsert;
