import { point } from '@/db/schema/point';

export type Point = {
	x: number;
	y: number;
};

export type FetchedPoint = typeof point.$inferSelect;
export type InsertPoint = typeof point.$inferInsert;
