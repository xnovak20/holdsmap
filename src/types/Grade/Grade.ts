import { ScaleName } from '@/types/Grade/scaleName';
import { V_SCALE } from '@/types/Grade/scales/vScale';
import { V_SCALE_PLUS } from '@/types/Grade/scales/vScalePlus';
import { FONT_SCALE } from '@/types/Grade/scales/fontScale';
import { Scale } from '@/types/Grade/scale';

export type GradeFromValue = {
	value: number | null;
};
export type GradeFromGrade = {
	grade: string;
	scale: ScaleName;
};
// just workaround for inability to have two constructors in typescript
export type GradeConstructorParams = GradeFromValue | GradeFromGrade;

export class Grade {
	private gradeValue: number | null;

	constructor(params: GradeConstructorParams) {
		if ('value' in params) {
			this.gradeValue = params.value;
		} else if ('grade' in params && 'scale' in params) {
			const { grade, scale } = params;
			this.gradeValue = this.setFromGrade(grade, scale);
		} else {
			throw new Error(` is not supported`);
		}
	}

	public getValue(): number {
		//todo grade muze byt i null, to by znamenalo, ze cesta jeste neni prelezena a jeji obtiznost neni znama. Provizorne dana nula
		return this.gradeValue ?? 0;
	}

	public setFromValue(value: number): void {
		this.gradeValue = value;
	}

	public setFromGrade(gradeName: string, scale: ScaleName): number {
		let value: number | undefined;

		switch (scale) {
			case ScaleName.VScale:
				value = V_SCALE[gradeName];
				break;
			case ScaleName.VScalePlus:
				value = V_SCALE_PLUS[gradeName];
				break;
			case ScaleName.Font:
				value = FONT_SCALE[gradeName];
				break;
			default:
				throw new Error(`ScaleName ${scale} is not supported`);
		}

		if (value !== undefined) {
			this.gradeValue = value;
			return this.gradeValue;
		} else {
			throw new Error(`Grade ${gradeName} not found in scale ${scale}`);
		}
	}

	public toString(scale: ScaleName): string {
		return this.getClosestGrade(this.getScaleValues(scale));
	}

	private getClosestGrade(scaleValues: Scale): string {
		if (this.gradeValue == null) {
			return '??';
		}
		let closestGrade: string | null = null;
		let closestDifference: number | null = null;

		for (const grade in scaleValues) {
			if (Object.prototype.hasOwnProperty.call(scaleValues, grade)) {
				const difference = Math.abs(
					scaleValues[grade] - this.gradeValue
				);
				if (
					closestDifference === null ||
					difference < closestDifference
				) {
					closestGrade = grade;
					closestDifference = difference;
				}
			}
		}

		if (closestGrade === null) {
			throw new Error('No closest grade found');
		}

		return closestGrade;
	}

	public getScaleValues(scale: ScaleName) {
		switch (scale) {
			case ScaleName.VScale:
				return V_SCALE;
			case ScaleName.VScalePlus:
				return V_SCALE_PLUS;
			case ScaleName.Font:
				return FONT_SCALE;
			default:
				throw new Error(`ScaleName ${scale} is not supported`);
		}
	}
}
