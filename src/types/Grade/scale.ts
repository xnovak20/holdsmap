export interface Scale {
	[gradeName: string]: number;
}
