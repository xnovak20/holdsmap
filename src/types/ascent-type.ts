import { ascent } from '@/db/schema/ascent';

export type FetchedAscent = typeof ascent.$inferSelect;
export type InsertAscent = typeof ascent.$inferInsert;
