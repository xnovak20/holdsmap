import { holdToRoute } from '@/db/schema/hold-to-route';

export type FetchedHoldToRoute = typeof holdToRoute.$inferSelect;
export type InsertHoldToRoute = typeof holdToRoute.$inferInsert;
