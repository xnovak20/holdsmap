import { wall } from '@/db/schema/wall';
import { Hold } from '@/types/hold-type';

export type InsertWall = typeof wall.$inferInsert;
export type FetchedWall = typeof wall.$inferSelect;

export type Wall = FetchedWall & {
	holds: Hold[];
};
