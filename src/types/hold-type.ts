import { Coordinates } from '@/types/coordinates-type';
import { hold } from '@/db/schema/hold';
import { HoldRole } from '@/types/hold-role-type';

export type FetchedHold = typeof hold.$inferSelect;
export type InsertHold = typeof hold.$inferInsert;

export type Hold = FetchedHold & {
	polygon: Coordinates[];
};

export type RouteHold = Hold & {
	role: HoldRole;
};
