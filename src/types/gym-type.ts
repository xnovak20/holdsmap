import { gym } from '@/db/schema/gym';

export type FetchedGym = typeof gym.$inferSelect;
export type InsertGym = typeof gym.$inferInsert;
