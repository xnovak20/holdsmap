'use server';

import { revalidatePath } from 'next/cache';
import ascentRepository from '@/repository/ascent-repository';

export const updateAscentAction = async (
	routeId: string,
	userId: string,
	attemptCount: number
) => {
	if (attemptCount > 0) {
		await ascentRepository.updateAttempts(routeId, userId, attemptCount);
		revalidatePath('/', 'layout');
	}
	return;
};
