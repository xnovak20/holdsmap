'use server';

import { FetchedWall } from '@/types/wall-type';
import wallRepository from '@/repository/wall-repository';

export const getAllWallsByGymIdAction = async (
	gymId: string
): Promise<FetchedWall[]> => {
	try {
		return await wallRepository.getAllWallsByGymId(gymId);
	} catch (error) {
		console.error('Error fetching walls:', error);
		return [];
	}
};
