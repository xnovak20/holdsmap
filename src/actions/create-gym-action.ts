'use server';

import { InferInsertModel } from 'drizzle-orm';
import { gym } from '@/db/schema/gym';
import { revalidatePath } from 'next/cache';
import { db } from '@/db';
import { v4 as uuidv4 } from 'uuid';
import { redirect } from 'next/navigation';
import { getAuthenticatedUser } from '@/auth';

export async function createGymAction(
	data: Omit<InferInsertModel<typeof gym>, 'id'>
) {
	const user = await getAuthenticatedUser();

	const id = uuidv4();

	await db.insert(gym).values({ id, ...data, ownerId: user.id });

	revalidatePath('/', 'layout');

	redirect(`/gyms/${id}`);
}
