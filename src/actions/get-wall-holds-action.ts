'use server';

import { Hold } from '@/types/hold-type';
import holdsRepository from '../repository/hold-repository';

export const getWallHoldsAction = async (wallId: string): Promise<Hold[]> => {
	try {
		return await holdsRepository.getHoldsFromWall(wallId);
	} catch (error) {
		console.error('Error fetching holds:', error);
		return [];
	}
};
