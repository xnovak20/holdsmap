'use server';

import { revalidatePath } from 'next/cache';
import ascentRepository from '@/repository/ascent-repository';

export const logSendAction = async (
	routeId: string,
	userId: string,
	attemptCount: number,
	stars: number,
	gradeOpinion: number
) => {
	if (attemptCount > 0) {
		await ascentRepository.logSend(
			routeId,
			userId,
			attemptCount,
			stars,
			gradeOpinion
		);
		revalidatePath('/', 'layout');
	}
	return;
};
