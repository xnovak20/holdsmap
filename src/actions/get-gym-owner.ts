'use server';

import { getAuthenticatedUser } from '@/auth';
import { db } from '@/db';
import { gymToUserOwnership } from '@/db/schema/gym-to-user-ownership';
import { and, eq } from 'drizzle-orm';

export const GetGymOwnership = async (gymId: string) => {
	const user = await getAuthenticatedUser();
	const succ = await db
		.select()
		.from(gymToUserOwnership)
		.where(
			and(
				eq(gymToUserOwnership.gymId, gymId),
				eq(gymToUserOwnership.userId, user.id)
			)
		);
	return succ.length !== 0;
};
