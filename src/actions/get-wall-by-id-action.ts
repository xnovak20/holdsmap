'use server';

import { FetchedWall } from '@/types/wall-type';
import wallRepository from '@/repository/wall-repository';

export const getWallByIdAction = async (
	wallId: string
): Promise<FetchedWall | null> => {
	try {
		return await wallRepository.getWallById(wallId);
	} catch (error) {
		console.error('Error fetching wall:', error);
		return null;
	}
};
