'use server';

import { db } from '@/db';
import { ascent } from '@/db/schema/ascent';
import { gym } from '@/db/schema/gym';
import { gymToUserBookmark } from '@/db/schema/gym-to-user-bookmark';
import { hold } from '@/db/schema/hold';
import { point } from '@/db/schema/point';
import { route } from '@/db/schema/route';
import { routeToUserBookmark } from '@/db/schema/route-to-user-bookmark';
import { wall } from '@/db/schema/wall';
import { eq } from 'drizzle-orm';
import { revalidatePath } from 'next/cache';

const DeleteGymAction = async (id: string) => {
	try {
		//todo tady stacil jen soft delete :facepalm:

		// Find all walls associated with the gym
		const relatedWalls = await db
			.select()
			.from(wall)
			.where(eq(wall.gymId, id));

		// For each wall, find all routes and holds associated with it
		for (const w of relatedWalls) {
			const relatedRoutes = await db
				.select()
				.from(route)
				.where(eq(route.wallId, w.id));

			// Delete related entries in the 'routeToUserBookmark' and 'ascent' tables
			for (const r of relatedRoutes) {
				await db
					.delete(routeToUserBookmark)
					.where(eq(routeToUserBookmark.routeId, r.id));
				await db.delete(ascent).where(eq(ascent.routeId, r.id));
			}

			// Delete routes associated with the wall
			await db.delete(route).where(eq(route.wallId, w.id));

			// Find holds associated with the wall
			const relatedHolds = await db
				.select()
				.from(hold)
				.where(eq(hold.wallId, w.id));

			// Delete related entries in the 'point' table
			for (const h of relatedHolds) {
				await db.delete(point).where(eq(point.holdId, h.id));
			}

			// Delete holds associated with the wall
			await db.delete(hold).where(eq(hold.wallId, w.id));
		}

		// Delete walls associated with the gym
		await db.delete(wall).where(eq(wall.gymId, id));

		// Delete related entries in the 'gymToUserBookmark' table
		await db
			.delete(gymToUserBookmark)
			.where(eq(gymToUserBookmark.gymId, id));

		// Finally, delete the gym itself
		await db.delete(gym).where(eq(gym.id, id));

		// Revalidate path
		revalidatePath('/', 'layout');

		// Return success response
		return true;
	} catch (error) {
		// Return error response
		return false;
	}
};

export default DeleteGymAction;
