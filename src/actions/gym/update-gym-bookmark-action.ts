'use server';

import { revalidatePath } from 'next/cache';
import gymToUserBookmarkRepository from '@/repository/gym-to-user-bookmark-repository';

export const updateGymBookmarkAction = async (
	userId: string,
	gymId: string
) => {
	try {
		const bookmark = await gymToUserBookmarkRepository.getGymBookmark(
			gymId,
			userId
		);
		if (bookmark.length === 0) {
			await gymToUserBookmarkRepository.addGymBookmark(gymId, userId);
		} else {
			await gymToUserBookmarkRepository.removeGymBookmark(gymId, userId);
		}
		revalidatePath('/gyms');
		return;
	} catch (error) {
		console.error('Error updating gym bookmark:', error);
		return null;
	}
};
