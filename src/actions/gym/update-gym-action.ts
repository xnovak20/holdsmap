'use server';

import { GymEditFormSchema } from '@/components/gym/gym-edit-form';
import { db } from '@/db';
import { gym } from '@/db/schema/gym';
import { eq } from 'drizzle-orm';
import { revalidatePath } from 'next/cache';
import { redirect } from 'next/navigation';

const updateGymAction = async (id: string, data: GymEditFormSchema) => {
	const updatedGym = await db
		.update(gym)
		.set(data)
		.where(eq(gym.id, id))
		.returning();

	if (updatedGym.length === 0) {
		throw new Error('Failed to update the gym.');
	}

	revalidatePath('/gyms', 'layout');

	redirect(`/gyms/${id}`);
};

export default updateGymAction;
