'use server';

import routeRepository from '../repository/route-repository';
import { RouteHold } from '@/types/hold-type';

export const addRouteAction = async (
	name: string,
	description: string | undefined,
	grade: number,
	wallId: string,
	authorId: string,
	holds: RouteHold[]
): Promise<string> => {
	try {
		return await routeRepository.createRoute(
			name,
			description ?? '',
			grade,
			wallId,
			authorId,
			holds
		);
	} catch (error) {
		console.error('Error adding route:', error);
		return '';
	}
};
