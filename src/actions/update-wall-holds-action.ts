'use server';

import { FetchedHold, Hold } from '@/types/hold-type';
import holdsRepository from '@/repository/hold-repository';
import holdRepository from '@/repository/hold-repository';
import { revalidatePath } from 'next/cache';

export const updateWallHoldsAction = async (
	wallId: string,
	gymId: string,
	updatedHolds: Hold[]
): Promise<boolean> => {
	try {
		const holdsOnWall: FetchedHold[] =
			await holdsRepository.getHoldsFromWall(wallId);

		//holdsToRemove == holdsOnWall - updatedHolds
		const holdsToRemove = holdsOnWall.filter(
			(hold) =>
				!updatedHolds.some((updatedHold) => hold.id === updatedHold.id)
		);
		await holdsRepository.deleteHolds(holdsToRemove);

		//holdsToAdd == updatedHolds - holdsOnWall
		const holdsToAdd = updatedHolds.filter(
			(hold) =>
				!holdsOnWall.some((updatedHold) => hold.id === updatedHold.id)
		);
		for (const hold of holdsToAdd) {
			await holdRepository.createHold(wallId, hold.type, hold.polygon);
		}
		revalidatePath(`/gyms/${gymId}/walls/${wallId}/edit/holds`, 'page');
		return true;
	} catch (error) {
		console.error('Error updating wall holds:', error);
		return false;
	}
};
