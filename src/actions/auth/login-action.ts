'use server';

import { signIn } from '@/auth';
import { BuiltInProviderType } from '@auth/core/providers';
import { revalidatePath } from 'next/cache';
import { redirect } from 'next/navigation';

export const loginAction = async <TCredentials extends Record<string, string>>(
	provider: BuiltInProviderType | string,
	credentials?: TCredentials
) => {
	await signIn(provider, credentials);

	revalidatePath('/', 'layout');

	redirect('/gyms');
};
