'use server';

import { signOut } from '@/auth';
import { redirect } from 'next/navigation';
import { revalidatePath } from 'next/cache';
import app from '@/config/app';

export const logoutAction = async () => {
	await signOut();

	revalidatePath('/', 'layout');

	redirect(app.notAuthenticatedRedirectPath);
};
