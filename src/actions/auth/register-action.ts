'use server';
import { RegisterFormSchema } from '@/components/auth/register-form';
import userRepository from '@/repository/user-repository';
import { redirect } from 'next/navigation';

export const registerAction = async (data: RegisterFormSchema) => {
	const credentials = { email: data.email, password: data.password };

	// Find user in database
	const user = await userRepository.matchCredentials(credentials);

	// Check if user exists in the database
	if (user) {
		throw new Error('This user already exists.');
	}

	// Register an account
	if (!(await userRepository.create(data))) {
		throw new Error('Unable to create a user.');
	}

	redirect('/auth/login');
};
