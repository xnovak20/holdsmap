'use server';

import { z } from 'zod';
import gymRepository from '@/repository/gym-repository';

const schema = z.object({
	searchPrompt: z.string({
		invalid_type_error: 'Invalid Prompt',
	}),
	userId: z.string({
		invalid_type_error: 'Invalid Id',
	}),
});

export const getGymListAction = async (
	userId: string,
	searchPrompt: string
) => {
	const validatedFields = schema.safeParse({
		searchPrompt: searchPrompt,
		userId: userId,
	});

	if (!validatedFields.success) {
		return {
			errors: validatedFields.error.flatten().fieldErrors,
		};
	}
	try {
		return {
			bookmarkedGyms: await gymRepository.getAllBookmarkedGymsByUser(
				userId,
				searchPrompt
			),
			nonBookmarkedGyms:
				await gymRepository.getAllNonBookmarkedGymsByUser(
					userId,
					searchPrompt
				),
		};
	} catch (error) {
		console.error('Error getting gyms:', error);
		return null;
	}
};
