'use server';

import { db } from '@/db';
import { wall } from '@/db/schema/wall';
import { eq } from 'drizzle-orm';
import { revalidatePath } from 'next/cache';
import { ResultMessage } from '@/types/result-message';

export const DeleteWallAction = async (id: string): Promise<ResultMessage> => {
	try {
		//todo delete the image from storage

		// Delete the wall
		await db.delete(wall).where(eq(wall.id, id)); //todo kua proc tu neni soft delete, proc tu neni normalne volany repozitar
		revalidatePath('/gyms', 'layout');

		// Return success response
		return { success: 'Wall deleted successfully.' };
	} catch (error) {
		// Return error response
		return { error: `Failed to delete the wall: ${error}` };
	}
};
