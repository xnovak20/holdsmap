'use client';
import {
	createContext,
	Dispatch,
	PropsWithChildren,
	SetStateAction,
	useContext,
	useState,
} from 'react';
import { Hold } from '@/types/hold-type';
import { FetchedWall } from '@/types/wall-type';

type EditHoldsContextType = {
	wall: FetchedWall;
	setWallHolds: Dispatch<SetStateAction<Hold[]>>;
	wallHolds: Hold[];
	setWall: Dispatch<SetStateAction<FetchedWall>>;
};

const wallPlaceholder: FetchedWall = {
	id: '',
	name: '',
	picture: '',
	createdAt: new Date(),
	deletedAt: null,
	gymId: null,
};

const EditHoldsContext = createContext<EditHoldsContextType>({
	wallHolds: [],
	setWallHolds: (): Hold[] => [],
	wall: wallPlaceholder,
	setWall: (): FetchedWall => wallPlaceholder,
});

export function EditHoldsContextProvider(props: PropsWithChildren) {
	const [wallHolds, setWallHolds] = useState<Hold[]>([]);
	const [wall, setWall] = useState<FetchedWall>(wallPlaceholder);

	return (
		<EditHoldsContext.Provider
			value={{ wall, setWall, wallHolds, setWallHolds }}
		>
			{props.children}
		</EditHoldsContext.Provider>
	);
}

export const useEditHoldsContext = () => useContext(EditHoldsContext);
