'use client';
import { createContext, PropsWithChildren, useState } from 'react';
import { Hold, RouteHold } from '@/types/hold-type';

type NewRouteContextType = {
	wallId: string;
	wallHolds: Hold[];
	newRouteHolds: RouteHold[];
	wallImage: string;
};

export const NewRouteContext = createContext<NewRouteContextType>({
	newRouteHolds: [],
	wallHolds: [],
	wallId: '',
	wallImage: '',
});

export function NewRouteContextProvider({ children }: PropsWithChildren) {
	const [newRoute, _setNewRoute] = useState<NewRouteContextType>({
		newRouteHolds: [],
		wallHolds: [],
		wallId: '',
		wallImage: '',
	});

	return (
		<NewRouteContext.Provider value={newRoute}>
			{children}
		</NewRouteContext.Provider>
	);
}
