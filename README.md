# Holds Map

## 🚀 Quickstart

-   Install all dependencies

```shell
npm install
```

-   Start the program

```shell
npm run start
```

## 🗃 Database

This project uses [Turso](https://turso.tech/) with [Drizzle ORM](https://orm.drizzle.team/).

### Migrations

You can skip migrations using [push](https://orm.drizzle.team/kit-docs/commands#prototype--push) command:

```shell
drizzle-kit push
```

[Learn more](https://orm.drizzle.team/kit-docs/commands#generate-migrations) about migrations using Drizzle ORM.

#### Generate migrations

```shell
drizzle-kit generate
```

#### Applying migrations

```shell
drizzle-kit migrate
```

### Instance

As the database is running via **Turso** you can either use `.env` configuration file to make configuration to the
turso database, or use [local database](https://docs.turso.tech/local-development) using the following command:

```shell
turso dev
```

### Drizzle Studio

For the local database view Drizzle provides the [Drizzle Studio](https://orm.drizzle.team/drizzle-studio/overview):

```shell
drizzle-kit studio
```

## 🐗 Authors

-   Roman Országh
-   Tadeáš Kachyňa
-   David Novák
-   Petr Hýbl
